#ifndef C_CONTA_CORRENTE_H
#define C_CONTA_CORRENTE_H

#include <iostream>
#include <string>
#include <cmath>
#include <cstring>

using namespace std;

class Conta_Corrente {

    string nome_do_correntista;

    int numero_da_conta;

    double valor_deposito;
    double saldo;
    double valor_saque = 0;

public:
    Conta_Corrente() = default;

    string altera_nome();

    int numero_conta();

    double deposito();

    double saque();

    void extrato();
};


#endif //C_CONTA_CORRENTE_H
