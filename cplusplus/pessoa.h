#ifndef C_PESSOA_H
#define C_PESSOA_H

#include <iostream>
#include <string>

using namespace std;

class pessoa {

    string nome;

    int idade;


    float peso;
    float altura = 0;

public:
    pessoa() = default;

    void envelhercer();

    void engordar();

    void emagrecer();

    void crescer();
};


#endif //C_PESSOA_H
