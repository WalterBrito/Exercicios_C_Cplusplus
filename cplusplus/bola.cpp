/*
Classe Bola: Crie uma classe que modele uma bola:

a. Atributos: Cor, circunferência, material
b. Métodos: trocaCor e mostraCor
*/

#include "bola.h"

void Bola::trocaCor() {

    cout << "Cor da bola: ";
    getline(cin, cor);

    cout << "Material: ";
    getline(cin, material);

    cout << "Circunferência: ";
    cin >> circunferencia;
}

void Bola::mostraCor() {
    cout << "\nCor da bola: " << cor << "\n";
    cout << "Circunferência: " << circunferencia << "\n";
    cout << "Material: " << material << "\n";
}

int main() {
    Bola bola;

    bola.trocaCor();
    bola.mostraCor();
}