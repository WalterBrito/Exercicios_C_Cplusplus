#ifndef C_QUADRADO_H
#define C_QUADRADO_H

#include <iostream>
#include <string>

using namespace std;

class quadrado {

    int tamanho_lado;
    int area;

public:
    quadrado() = default;

    void mudar_valor_lado();

    void calcula_area();

    void retorna_valor_lado();
};

#endif //C_QUADRADO_H
