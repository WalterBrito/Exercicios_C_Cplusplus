/*
Classe Pessoa: Crie uma classe que modele uma pessoa:
a. Atributos: nome, idade, peso e altura
b. Métodos: Envelhercer, engordar, emagrecer, crescer. Obs: Por
padrão, a cada ano que nossa pessoa envelhece, sendo a idade
dela menor que 21 anos, ela deve crescer 0,5 cm.
*/

#include "pessoa.h"

void pessoa::envelhercer() {
    cout << "Nome: ";
    getline(cin, nome);

    cout << "Idade: ";
    cin >> idade;
}

void pessoa::engordar() {
    cout << "Peso: ";
    cin >> peso;
}

void pessoa::emagrecer() {

}

void pessoa::crescer() {

    for (int i = 0; i < idade; ++i) {
        if (idade < 21) {
            altura += 0.5;
        }
    }

    cout << "\nNome: " << nome << endl;
    cout << "Idade: " << idade << endl;
    cout << "Peso: " << peso << endl;
    cout << "Altura: " <<  altura << endl;
}


int main() {
    pessoa p;

    p.envelhercer();
    p.engordar();
    p.emagrecer();
    p.crescer();
}