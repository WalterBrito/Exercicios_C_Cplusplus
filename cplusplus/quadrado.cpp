/*
Classe Quadrado: Crie uma classe que modele um quadrado:
a. Atributos: Tamanho do lado
b. Métodos: Mudar valor do Lado, Retornar valor do Lado e calcular
Área;
*/

#include "quadrado.h"

void quadrado::mudar_valor_lado() {
    cout << "Valor do lado: ";
    cin >> tamanho_lado;
}

void quadrado::calcula_area() {
    area = tamanho_lado * tamanho_lado;
}

void quadrado::retorna_valor_lado() {
    cout << "Tamanho do quadrado: " << area << endl;
}


int main() {
    quadrado q;

    q.mudar_valor_lado();
    q.calcula_area();
    q.retorna_valor_lado();
}