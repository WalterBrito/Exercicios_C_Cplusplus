/*
Classe Retangulo: Crie uma classe que modele um retangulo:

a. Atributos: LadoA, LadoB (ou Comprimento e Largura, ou Base e
Altura, a escolher)
b. Métodos: Mudar valor dos lados, Retornar valor dos lados,
calcular Área e calcular Perímetro;
c. Crie um programa que utilize esta classe. Ele deve pedir ao
usuário que informe as medidades de um local. Depois, deve criar
um objeto com as medidas e calcular a quantidade de pisos e de
rodapés necessárias para o local.
*/

#include "retangulo.h"

void retangulo::mudar_valor_lado() {
    cout << "Base: ";
    cin >> base;

    cout << "Altura: ";
    cin >> altura;
}

void retangulo::calcula_area() {

    area = base * altura;
}

void retangulo::retorna_valor_lado() {
    cout << "Área do retângulo: " << area << endl;
}

void retangulo::calcula_perimetro() {
    perimetro = 2 * (base + altura);

    cout << "Perimetro: " << perimetro << endl;
}

int main() {

    retangulo r;

    r.mudar_valor_lado();
    r.calcula_area();
    r.retorna_valor_lado();
    r.calcula_perimetro();
}