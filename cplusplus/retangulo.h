#ifndef C_RETANGULO_H
#define C_RETANGULO_H

#include <iostream>
#include <string>

using namespace std;

class retangulo {

public:
    retangulo() = default;

    void mudar_valor_lado();

    void calcula_area();

    void retorna_valor_lado();

    void calcula_perimetro();
};

#endif //C_RETANGULO_H;
