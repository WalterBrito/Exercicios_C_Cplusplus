/*+
Classe Conta Corrente: Crie uma classe para implementar uma conta corrente. A
classe deve possuir os seguintes atributos: número da conta, nome do correntista e
saldo. Os métodos são os seguintes: alterarNome, depósito e saque; No construtor,
saldo é opcional, com valor default zero e os demais atributos são obrigatórios.
*/


#include "Conta_Corrente.h"

string Conta_Corrente::altera_nome() {
    cout << "Nome do correntista: ";
    getline(cin, nome_do_correntista);
};

int Conta_Corrente::numero_conta() {

    cout << "Número da conta: ";
    cin >> numero_da_conta;
}

double Conta_Corrente::deposito() {

    cout << "Valor do depósito: ";
    cin >> valor_deposito;
};

double Conta_Corrente::saque() {

    cout << "Saque: ";
    cin >> valor_saque;
};

void Conta_Corrente::extrato() {

    cout << "\t\tExtrato\n\n";
    cout << "Nome do correntista: " << nome_do_correntista << endl;
    cout << "Número da conta: " << numero_da_conta << endl;
    cout << "Valor do depósito: " << valor_deposito << endl;
    cout << "Valor do saque: " << valor_saque << endl;
};

int main() {

    Conta_Corrente c;

    c.altera_nome();
    c.numero_conta();
    c.deposito();
    c.saque();
    c.extrato();
}