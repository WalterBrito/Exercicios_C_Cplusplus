/*
Faça um programa que calcule o fatorial de um número inteiro fornecido pelo
usuário. Ex.: 5!=5.4.3.2.1=120. A saída deve ser conforme o exemplo abaixo:
Fatorial de: 5
5! = 5 . 4 . 3 . 2 . 1 = 120
*/

#include <stdio.h>

int num;
int i;
int x;
int y;
int resultado = 0;

int main() {
    printf("Digite um número: ");
    scanf("%d", &num);

    printf("\n%d! = ", num);
    for (i = 0; num > i; i++) {
        printf("%d ", num-i);
        // multilplica número digitado pelo usuário e subtrai o mesmo por menos um
        // 5 x 4...
        x = num * (num-i);

        // pega o resultado anterior e e faz o mesmo processo de multiplicação
        // 20 x 3...
        y = x * (num-i);

        resultado += y;
    }

    printf("= %d\n", resultado);
}