/*
Faça um programa para uma loja de tintas. O programa deverá pedir o tamanho
em metros quadrados da área a ser pintada. Considere que a cobertura da tinta é
de 1 litro para cada 3 metros quadrados e que a tinta é vendida em latas de 18
litros, que custam R$ 80,00. Informe ao usuário a quantidades de latas de tinta a
serem compradas e o preço total.
*/

#include <stdio.h>
#include <stdlib.h>

int tamanho_metros;
int cobertura_de_tinta;
int latas_a_comprar;
int latas_vendidas = 18;
double preco_lata_tinta = 80.00;
double preco_total;

void tintas() {
    printf("Tamanho da área a ser pintada?: ");
    scanf("%d", &tamanho_metros);

    cobertura_de_tinta = (tamanho_metros / 3);
    latas_a_comprar = (cobertura_de_tinta / latas_vendidas);
    preco_total = latas_a_comprar * preco_lata_tinta;

    printf("\n%d lata(s) de tintas deverão ser compradas.", latas_a_comprar);
    printf("\nPreço total: R$ %.2lf\n", preco_total);
}

int main() {
    tintas();
}