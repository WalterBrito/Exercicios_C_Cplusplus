/*
As Organizações Tabajara resolveram dar um abono aos seus colaboradores em
reconhecimento ao bom resultado alcançado durante o ano que passou. Para isto
contratou você para desenvolver a aplicação que servirá como uma projeção de
quanto será gasto com o pagamento deste abono.
Após reuniões envolvendo a diretoria executiva, a diretoria
financeira e os representantes do sindicato laboral, chegou-se a
seguinte forma de cálculo:

a.Cada funcionário receberá o equivalente a 20% do seu salário bruto
de dezembro;
a.O piso do abono será de 100 reais, isto é, aqueles funcionários
cujo salário for muito baixo, recebem este valor mínimo;

Neste momento, não se deve ter nenhuma preocupação com colaboradores
com tempo menor de casa, descontos, impostos ou outras
particularidades. Seu programa deverá permitir a digitação do salário
de um número indefinido (desconhecido) de salários. Um valor de
salário igual a 0 (zero) encerra a digitação. Após a entrada de todos
os dados o programa deverá calcular o valor do abono concedido a cada
colaborador, de acordo com a regra definida acima. Ao final, o
programa deverá apresentar:

a. O salário de cada funcionário, juntamente com o valor do abono;
b. O número total de funcionário processados;
c. O valor total a ser gasto com o pagamento do abono;
d. O número de funcionário que receberá o valor mínimo de 100 reais;
e. O maior valor pago como abono;
A tela abaixo é um exemplo de execução do programa, apenas para fins
ilustrativos. Os valores podem mudar a cada execução do programa.

Projeção de Gastos com Abono
============================

Salário: 1000
Salário: 300
Salário: 500
Salário: 100
Salário: 4500
Salário: 0

Salário             Abono
R$ 1000.00   -      R$ 200.00
R$ 300.00    -      R$ 100.00
R$ 500.00    -      R$ 100.00
R$ 100.00    -      R$ 100.00
R$ 4500.00   -      R$ 900.00

Foram processados 5 colaboradores
Total gasto com abonos: R$ 1400.00
Valor mínimo pago a 3 colaboradores
Maior valor de abono pago: R$ 900.00
*/

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#define SIZE 10

// Declaração de função
void projecao_de_gasto();

// 20% do salário de dezembro
int porcentagem_salario = 20;
int i = 0;
int colaborador = 0;
// Conta quantos colaboradores receberam o valor minimo
int valor_minimo_cont = 0;

double abono;
double abono_aux[SIZE];
// Valor total gasto com abono
double valor_gasto_abono = 0;
double salario;
// Add o valor do salário
double salario_aux[SIZE];
// Valor do salário de dezembro
double aux;
// Maior valor de abono pago
double maior_valor = 0;
double menor_valor = 0;


int main() {
    projecao_de_gasto();
}

void projecao_de_gasto() {

    puts("Projeção de Gastos com Abono");
    puts("============================");
    while (true) {
        printf("Salário: ");
        scanf("%lf", &salario);

        if (salario == 0) {
            break;
        }

        // Add salário no vetor
        salario_aux[i] = (int) salario;

        // Add valores de abono
        if (salario < 1000) {
            abono_aux[i] = 100;
        }

        if (salario > 1000) {
            abono_aux[i] = 200;
        }

        // Conta a quantidade de colaboradores
        colaborador++;
        i++;
    }

    puts("\nSalário\t\t\tAbono");

    for (int k = 0; k < i; ++k) {
        printf("R$ %.2lf\t - \tR$ %.2lf\n", salario_aux[k], abono_aux[k]);
    }

    printf("\nForam processados %d colaboradores\n", colaborador);

    for (int j = 0; j < i; ++j) {
        valor_gasto_abono += abono_aux[j];
    }

    printf("Total gasto com abonos: R$ %.2lf\n", valor_gasto_abono);

    for (int m = 0; m < i; ++m) {
        if (salario_aux[m] < 1000) {
            valor_minimo_cont++;
        }
    }

    printf("Valor mínimo pago a %d colaboradores\n", valor_minimo_cont);


    for (int l = 0; l < abono_aux[i]; ++l) {

        if (maior_valor > menor_valor) {
            maior_valor = abono_aux[l];
        }
    }

    printf("Maior valor de abono pago: R$ %.2lf\n", maior_valor);

}

