/*
Um funcionário de uma empresa recebe aumento salarial anualmente: Sabe-se
que:
a. Esse funcionário foi contratado em 1995, com salário inicial de
R$ 1.000,00;
b. Em 1996 recebeu aumento de 1,5% sobre seu salário inicial;
c. A partir de 1997 (inclusive), os aumentos salariais sempre
correspondem ao dobro do percentual do ano anterior.
Faça um programa que determine o salário atual desse funcionário.

Após concluir isto, altere o programa permitindo que o usuário
digite o salário inicial do funcionário.
*/

#include <stdio.h>
#include <stdbool.h>

double salario_inicial;
double aumento = 1.05;
double dobro_percentual =  1.05;
double aumento_salarial;
double aux;

int main() {

    printf("Salário: ");
    scanf("%lf", &salario_inicial);

    printf("\nAumento Percentual (1996): %.2lf%%", aumento);
    for (int i = 1997; i <= 2018; i++) {
        aumento_salarial = aumento + dobro_percentual;
        aumento = dobro_percentual + aumento;
        dobro_percentual = aumento_salarial;
        printf("\nAumento Percentual (%d): %.2lf%%", i, aumento_salarial);
    }

    printf("\n\nSalário inicial (1995): R$ %.2lf", salario_inicial);
    printf("\nSalario Atual: R$ %.2lf\n", salario_inicial * aumento_salarial);
}

