/*
Faça um Programa que peça uma data no formato dd/mm/aaaa e determine se a
mesma é uma data válida.
*/

#include <stdlib.h>
#include <stdio.h>
#include <time.h>

int dia;
int mes;
int ano;

int main() {
    printf("Digite a dia: ");
    scanf("%d", &dia);

    printf("Digite o mês: ");
    scanf("%d", &mes);

    printf("Digite o ano: ");
    scanf("%d", &ano);

    if (dia <= 31 && mes <= 12 && (ano >= 1000)) {
        printf("\n%d/%d/%d\n", dia, mes, ano);
    } else {
        printf("\nFormato inválido.\n");
    }
}