/*
'''Palíndromo.''' Um palíndromo é uma seqüência de caracteres cuja leitura é
idêntica se feita da direita para esquerda ou vice−versa. Por exemplo: '''OSSO''' e
'''OVO''' são palíndromos. Em textos mais complexos os espaços e pontuação são
ignorados. A frase '''SUBI NO ONIBUS''' é o exemplo de uma frase palíndroma
onde os espaços foram ignorados. Faça um programa qu
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAMANHO 50

void main() {
    
    char palavra[20];

    int i, j = 0; // inicia com 0;

    printf("Digite uma palavra: ");
    scanf("%s", palavra);

//    fgets(palavra, TAMANHO, stdin);

    printf("Contrário: ");
    for (i = strlen(palavra); i >= 0; i--) {
        printf("%c", palavra[i]);

        if ((palavra[strlen(palavra)-1] == palavra[0]) && (palavra[strlen(palavra)-2] == palavra[1])) {
            j++;
        }
    }

    if(j != 0) {
      printf("\n\nPalindromo\n");
   } else {
      printf("\n\nNão é Palindromo\n");
   }
}