// Função para imprimir string na vertical

#ifndef EXERCICIOS_STRING_VERTICAL_H
#define EXERCICIOS_STRING_VERTICAL_H

#endif //EXERCICIOS_STRING_VERTICAL_H

#include <string.h>
#include <stdio.h>

void vertical(char x[20]) {

    char y[20];

    int i = 0;

    // Add string no vetor
    while (i < strlen(x)) {
        y[i] = x[i];
        i++;

    }

    // Percorre o vetor
    for (int j = 0; j < strlen(x); ++j) {
        printf("%c\n", x[j]);
    }
}