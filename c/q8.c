/*
Faça um Programa que pergunte quanto você ganha por hora e o número de horas
trabalhadas no mês. Calcule e mostre o total do seu salário no referido mês.
*/

#include <stdio.h>
#include <stdlib.h>

int main() {

    double ganho_hora;
    double horas_trabalhadas;
    double salario;

    printf("Valor da hora trabalhada: ");
    scanf("%lf", &ganho_hora);

    printf("Quantidade de horas trabalhadas: ");
    scanf("\n%lf", &horas_trabalhadas);

    salario = ganho_hora * horas_trabalhadas;
    printf("Valor do salário: %.2lf\n", salario);
}