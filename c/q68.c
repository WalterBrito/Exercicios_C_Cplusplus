/*
Faça um programa que peça para n pessoas a sua idade, ao final o programa
devera verificar se a média de idade da turma varia entre 0 e 25,26 e 60 e maior
que 60; e então, dizer se a turma é jovem, adulta ou idosa, conforme a média
calculada.
*/

#include <stdio.h>
#include <stdbool.h>

int idade;
int count;

double media = 0;

int main() {

    while (true) {

        printf("Digite a idade: ");
        scanf("%d", &idade);

        if (idade < 0) {
            break;
        }

        if (idade >= 0 && idade <= 25) {
            count++;
        } else if (idade >= 26 && idade <= 60) {
            count++;
        } else if (idade >= 60) {
            count++;
        }

        media += count;
    }

    if (media >= 0 && media <= 25) {
        printf("\nTurma jovem!\n");
        count++;
    } else if (media >= 26 && media <= 60) {
        printf("\nTurma adulta!\n");
        count++;
    } else if (media >= 60) {
        printf("\nTurma idosa!\n");
        count++;
    }
}