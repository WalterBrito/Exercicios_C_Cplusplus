/*
Faça um programa que peça uma nota, entre zero e dez. Mostre uma mensagem
caso o valor seja inválido e continue pedindo até que o usuário informe um valor
válido.
*/

#include <stdio.h>
#include <stdbool.h>

int main() {

double nota;

while (true) {
    printf("Digite um número: ");
    scanf("%lf", &nota);

    if (nota >= 0 && nota <= 10) {
        printf("\nNota %.1lf\n", nota);
        break;
    } else {
        printf("\nNúmero inválido.\n");
    }
  }
}