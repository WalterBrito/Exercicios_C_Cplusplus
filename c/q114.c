/*
Faça um programa que use a função valorPagamento para determinar o valor a ser
pago por uma prestação de uma conta. O programa deverá solicitar ao usuário o
valor da prestação e o número de dias em atraso e passar estes valores para a
função valorPagamento, que calculará o valor a ser pago e devolverá este valor ao
programa que a chamou. O programa deverá então exibir o valor a ser pago na
tela. Após a execução o programa deverá voltar a pedir outro valor de prestação e
assim continuar até que seja informado um valor igual a zero para a prestação.
Neste momento o programa deverá ser encerrado, exibindo o relatório do dia, que
conterá a quantidade e o valor total de prestações pagas no dia. O cálculo do valor
a ser pago é feito da seguinte forma. Para pagamentos sem atraso, cobrar o valor
da prestação. Quando houver atraso, cobrar 3% de multa, mais 0,1% de juros por
dia de atraso.
*/

#include <stdio.h>

#include "q114.h"

void resultado();

int main() {
    resultado();
}

void resultado() {

    int dias_de_atraso = 0;
    int i = 0;

    double valor_a_pagar = 0;
    double total = 0;

    while (1) {
        printf("Valor á pagar (Ex.: 32.50): ");
        scanf("%lf", &valor_a_pagar);

        if (valor_a_pagar == 0) {
            break;
        }

        printf("Dias em atraso (Ex.: 5): ");
        scanf("%d", &dias_de_atraso);

        printf("\nValor a ser pago: R$ %.2lf\n", valorPagamento(valor_a_pagar, dias_de_atraso));
        puts("---------------------------");
        total += valorPagamento(valor_a_pagar, dias_de_atraso);

        i++;
    }

    puts("\n---------------------------");
    puts("\tRelatório do dia");
    printf("Valor Total: R$ %.2lf\n", total);
}