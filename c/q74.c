/*
O Sr. Manoel Joaquim expandiu seus negócios para além dos negócios de 1,99 e
agora possui uma loja de conveniências. Faça um programa que implemente uma
caixa registradora rudimentar. O programa deverá receber um número
desconhecido de valores referentes aos preços das mercadorias. Um valor zero
deve ser informado pelo operador para indicar o final da compra. O programa
deve então mostrar o total da compra e perguntar o valor em dinheiro que o
cliente forneceu, para então calcular e mostrar o valor do troco. Após esta
operação, o programa deverá voltar ao ponto inicial, para registrar a próxima
compra. A saída deve ser conforme o exemplo abaixo:
Lojas Tabajara
Produto 1: R$ 2.20
Produto 2: R$ 5.80
Produto 3: R$ 0
Total: R$ 9.00
Dinheiro: R$ 20.00
Troco: R$ 11.00
...
*/

#include <stdio.h>
#include <stdbool.h>

int i = 1;

double valor_produto;
double dinheiro;
double total;
double troco;
double total_aux = 0;

int main() {

    while (true) {

        printf("Produto %d: ", i);
        scanf("%lf", &valor_produto);

        i++;

        if (valor_produto == 0) {
            break;
        }

        total_aux += valor_produto;
    }

    printf("\nDinheiro: ");
    scanf("%lf", &dinheiro);

    total = total_aux;
    troco = dinheiro - total;

    puts("\n\t\tLojas Tabajara");
    printf("\nTotal: R$ %.2lf", total);
    printf("\nDinheiro: R$ %.2lf", dinheiro);
    printf("\nTroco: R$ %.2lf\n", troco);
}