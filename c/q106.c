/*
Uma empresa de pesquisas precisa tabular os resultados da seguinte enquete feita
a um grande quantidade de organizações:
"Qual o melhor Sistema Operacional para uso em servidores?"
As possíveis respostas são:

1- Windows Server
2- Unix
3- Linux
4- Netware
5- Mac OS
6- Outro

Você foi contratado para desenvolver um programa que leia o
resultado da enquete e informe ao final o resultado da mesma. O
programa deverá ler os valores até ser informado o valor 0, que
encerra a entrada dos dados. Não deverão ser aceitos valores além
dos válidos para o programa (0 a 6). Os valores referentes a cada
uma das opções devem ser armazenados num vetor.
Após os dados terem sido completamente informados, o programa deverá
calcular a percentual de cada um dos concorrentes e informar o
vencedor da enquete. O formato da saída foi dado pela empresa, e é o
seguinte:

Sistema Operacional         Votos         %
-------------------         -----       -----
Windows Server              1500         17%
Unix                        3500         40%
Linux                       3000         34%
Netware                     500          5%
Mac OS                      150          2%
Outro                       150          2%
-------------------         ---         ---
Total                       8800

O Sistema Operacional mais votado foi o Unix, com 3500 votos,
correspondendo a 40% dos votos.
*/

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define SIZE 15
#define NUMERO_DE_CARACTERES 6

// Mostra no final o nome do sistema
char windows_nome[] = "Windows Server";
char unix_nome[] = "Unix";
char linux_nome[] = "Linux";
char netware_nome[] = "Netware";
char mac_os_nome[] = "Mac Os";
char outros_nome[] = "Outros";

// Pede o voto
int voto;
// Armazena os votos digitados
int voto_array[SIZE];

// Percentagem
int windows_perc;
int unix_perc;
int linux_perc;
int netware_perc;
int mac_os_perc;
int outros_perc;

// Incrementa o loop
int i = 0;

// Contadores
int windows = 0;
int unix_cont = 0;
int linux_cont = 0;
int netware = 0;
int mac_os = 0;
int outros = 0;

int percentagem;
int total = 0;

// Mais votado
int mais_votado;

// Declarada função
void enquete();

int maior(int a, int b, int c, int d, int e, int f);

char nome(int a, int b, int c, int d, int e, int f);

int main() {
    enquete();
}

void enquete() {
    puts("Qual o melhor Sistema Operacional para uso em servidores?");
    printf("1- Windows Server\n2- Unix\n3- Linux\n4- Netware\n5- Mac OS\n6- Outro\n");

    while (true) {

        printf("Digite a opção: ");
        scanf("%d", &voto);

        // Add cada valor no voto_array[]
        voto_array[i] = voto;

        if (voto == 0) {
            break;
        }

        if (voto == 1) {
            windows++;
        } else if (voto == 2) {
            unix_cont++;
        } else if (voto == 3) {
            linux_cont++;
        } else if (voto == 4) {
            netware++;
        } else if (voto == 5) {
            mac_os++;
        } else if (voto == 6) {
            outros++;
        } else {
            puts("Opção inválida.");
        }

        i++;
    }

    // Percentual
    windows_perc = windows / 100;
    unix_perc = unix_cont / 100;
    linux_perc = linux_cont / 100;
    netware_perc = netware / 100;
    mac_os_perc = mac_os / 100;
    outros_perc = outros / 100;

    // Total incrementa com contadores
    total += (windows + unix_cont + linux_cont + netware + mac_os + outros);

    printf("\nSistema Operacional\tVotos\t%%\n");
    printf("-------------------\t---\t---\n");
    printf("Windows Server\t\t%d\t%d\n", windows, windows_perc);
    printf("Unix\t\t\t%d\t%d\n", unix_cont, unix_perc);
    printf("Linux\t\t\t%d\t%d\n", linux_cont, linux_perc);
    printf("Netware\t\t\t%d\t%d\n", netware, netware_perc);
    printf("Mac OS\t\t\t%d\t%d\n", mac_os, mac_os_perc);
    printf("Outros\t\t\t%d\t%d\n", outros, outros_perc);
    printf("-------------------\t---\t---\n");
    printf("Total\t\t\t%d\t\t\n", total);

    if (maior(windows, unix_cont, linux_cont, netware, mac_os, outros) == windows) {
        percentagem = windows_perc;
    } else if (maior(windows, unix_cont, linux_cont, netware, mac_os, outros) == unix_cont) {
        percentagem = unix_perc;
    } else if (maior(windows, unix_cont, linux_cont, netware, mac_os, outros) == linux_cont) {
        percentagem = linux_perc;
    } else if (maior(windows, unix_cont, linux_cont, netware, mac_os, outros) == netware) {
        percentagem = netware_perc;
    } else if (maior(windows, unix_cont, linux_cont, netware, mac_os, outros) == mac_os) {
        percentagem = mac_os_perc;
    } else if (maior(windows, unix_cont, linux_cont, netware, mac_os, outros) == outros) {
        percentagem = outros_perc;
    }

    printf("\nO Sistema Operacional mais votado foi o %c com %d votos. correspondendo a %d%% dos votos.\n",
           nome(windows, unix_cont, linux_cont, netware, mac_os, outros),
           maior(windows, unix_cont, linux_cont, netware, mac_os, outros),
           percentagem);
}

int maior(int a, int b, int c, int d, int e, int f) {
    int maior = a;

    if (b > maior) {
        maior = b;
    } else if (c > maior) {
        maior = c;
    } else if (d > maior) {
        maior = d;
    } else if (e > maior) {
        maior = e;
    } else if (f > maior) {
        maior = f;
    }

    return maior;
}

char nome(int a, int b, int c, int d, int e, int f) {
    int maior = a;
    char n[NUMERO_DE_CARACTERES] = {'W', 'U', 'L', 'N', 'M', 'O'};

    for (int j = 0; j < sizeof(n[NUMERO_DE_CARACTERES]); ++j) {
        if (b > maior) {
            n[j] = n[1];
        } else if (c > maior) {
            n[j] = n[2];
        } else if (d > maior) {
            n[j] = n[3];
        } else if (e > maior) {
            n[j] = n[4];
        } else if (f > maior) {
            n[j] = n[5];
        } else if (a >= maior) {
            n[j] = n[0];
        }

        return n[j];
    }
}

