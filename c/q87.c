/*
Desenvolver um programa para verificar a nota do aluno em uma prova com 10
questões, o programa deve perguntar ao aluno a resposta de cada questão e ao
final comparar com o gabarito da prova e assim calcular o total de acertos e a nota
(atribuir 1 ponto por resposta certa). Após cada aluno utilizar o sistema deve ser
feita uma pergunta se outro aluno vai utilizar o sistema. Após todos os alunos
terem respondido informar:
a. Maior e Menor Acerto;
b. Total de Alunos que utilizaram o sistema;
c. A Média das Notas da Turma.
Gabarito da Prova:

01 - A
02 - B
03 - C
04 - D
05 - E
06 - E
07 - D
08 - C
09 - B
10 - A

Após concluir isto você poderia incrementar o programa permitindo
que o professor digite o gabarito da prova antes dos alunos usarem o
programa.
*/

#include <stdio.h>
#include <stdbool.h>

void prova();

int main() {
    prova();
}

void prova() {
    char questao;
    char gabarito;
    // Pergunta pelo que usará o sistema
    char aluno;

    // Conta quantos alunos usaram o sistema
    int aluno_cont = 0;
    // Conta os acertos
    int acertos = 0;
    // Conta os erros
    int erros = 0;
    // Conta as questões
    int q = 0;

    double media;

    for (int i = 1; i <= 10; i++) {
        printf("Gabarito %d°: ", i);
        scanf("%s", &gabarito);
    }

    puts("");

    while (true) {

        for (int j = 1; j <= 10; j++) {
            printf("%d° questão: ", j);
            scanf("%s", &questao);

            if (questao == gabarito) {
                acertos++;
            } else {
                erros++;
            }

            q++;
        }

        printf("\nDeseja continuar: ");
        scanf("%s", &aluno);

        puts("");

        if (aluno == 'n') {
            break;
        }

        aluno_cont++;
    }

    printf("\nMaior acerto: %d.", acertos);
    printf("\nMenor acerto: %d.", erros);
    printf("\nTotal de alunos: %d", aluno_cont);

    media = q / 4;
    printf("\nMedia: %.1lf\n",  media);
}