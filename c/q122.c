/*
'''Nome na vertical.''' Faça um programa que solicite o nome do usuário e
imprima-o na vertical.
F
U
L
A
N
O
*/

#include "string_vertical.h"

int main() {

    char palavra[20];

    printf("Digite uma palavra: ");
    scanf("%s", palavra);

    vertical(palavra);
}