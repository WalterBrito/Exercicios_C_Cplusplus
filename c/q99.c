/*
Faça um Programa que leia um vetor A com 10 números inteiros, calcule e mostre
a soma dos quadrados dos elementos do vetor.
*/

#include <stdio.h>
#include <math.h>

int main() {
    int a[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int b[10];
    int soma = 0;

    for (int i = 0; i < 10; ++i) {
        // Coloca os quadrados do vetor a no vetor b
        b[i] = a[i] * a[i];
    }

    for (int j = 0; j < 10; ++j) {
        // Soma os valores do vetor b e coloca em soma.
        soma += b[j];
    }

//    for (int k = 0; k < 10; ++k) {
//        printf("%d ", b[k]);
//    }

    // Mostra a soma dos quadrados no vetor b[]
    printf("\nSoma: %d \n", soma);

}