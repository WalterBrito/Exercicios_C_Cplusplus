/*
Faça um Programa que peça um número inteiro e determine se ele é par ou impar.
Dica: utilize o operador módulo (resto da divisão).
*/

#include <stdio.h>

int num;

int main() {
    printf("Digite um número: ");
    scanf("%d", &num);

    if (num % 2 == 0) {
        puts("Par");
    } else {
        puts("Impar");
    }
}