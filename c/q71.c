/*
Faça um programa que calcule o valor total investido por um colecionador em sua
coleção de CDs e o valor médio gasto em cada um deles. O usuário deverá
informar a quantidade de CDs e o valor para em cada um.
*/

#include <stdio.h>

int quantidade_cd;

double valor_cd;
double aux_cd = 0;
double valor_total;
double valor_medio_gasto;

int main() {

    printf("Quantidade de CDs: ");
    scanf("%d", &quantidade_cd);

    for (int i = 0; i < quantidade_cd; i++) {
        printf("Valor do CD: ");
        scanf("%lf", &valor_cd);

        aux_cd += valor_cd;
    }

    valor_total = aux_cd;
    valor_medio_gasto = valor_total / quantidade_cd;

    printf("\nValor Total: %.2lf", valor_total);
    printf("\nValor médio gasto: %.2lf\n", valor_medio_gasto);
}