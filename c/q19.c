/*
Faça um programa que peça o tamanho de um arquivo para download (em MB) e
a velocidade de um link de Internet (em Mbps), calcule e informe o tempo
aproximado de download do arquivo usando este link (em minutos).
*/

#include <stdio.h>
#include <stdlib.h>

int main() {
    double tamanho_arquivo;
    double velocidade;
    double tempo_download;

    printf("Tamanho do arquivo?: ");
    scanf("%lf", &tamanho_arquivo);

    printf("Qual a velocidade?: ");
    scanf("%lf", &velocidade);

    tempo_download = ((tamanho_arquivo / velocidade) * 60);

    printf("\nTempo aproximado: %.1lf minutos.\n", tempo_download);
}