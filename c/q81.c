/*
Faça um programa que leia dez conjuntos de dois valores, o primeiro
representando o número do aluno e o segundo representando a sua altura em
centímetros. Encontre o aluno mais alto e o mais baixo. Mostre o número do
aluno mais alto e o número do aluno mais baixo, junto com suas alturas.
*/

#include <stdio.h>

int numero;
int altura;

// função pré-declarada
int num_alto(int num);
int num_baixo(int num);
int maior_altura(int alto);
int menor_altura(int baixo);

int maior_altura(int alto) {
    int max = altura;

    if (alto > max) {
        max = altura;
    }

    return max;
}

int menor_altura(int baixo) {
    int min = altura;

    if (baixo < min) {
        min = altura;
    }

    return min;
}

int num_alto(int num) {
    int a = numero;

    if (num == maior_altura(a)) {
        a = numero;
    }

    return a;
}

int num_baixo(int num) {
    int b = numero;

    if (num == menor_altura(b)) {
        b = numero;
    }

    return b;
}

void conjunto() {
    for (int i = 1; i <= 10; i++) {
        printf("\t\tConjunto %d\n", i);

        printf("Número: ");
        scanf("%d", &numero);

        printf("Altura: ");
        scanf("%d", &altura);
    }

    printf("\nNúmero: %d, Maior: %d", num_alto(numero), maior_altura(altura));
    printf("\nNúmero: %d, Menor: %d\n", num_baixo(numero), menor_altura(altura));
}

int main() {
    conjunto();
}