/*
Faça um programa que lê as duas notas parciais obtidas por um aluno numa
disciplina ao longo de um semestre, e calcule a sua média. A atribuição de
conceitos obedece à tabela abaixo:

Média de Aproveitamento Conceito
Entre 9.0 e 10.0            A
Entre 7.5 e 9.0             B
Entre 6.0 e 7.5             C
Entre 4.0 e 6.0             D
Entre 4.0 e zero            E

O algoritmo deve mostrar na tela as notas, a média, o conceito correspondente e
a mensagem “APROVADO” se o conceito for A, B ou C ou “REPROVADO” se
o conceito for D ou E.
*/

#include <stdio.h>

double nota1;
double nota2;
double media;

void conceito() {
    printf("Nota 1°: ");
    scanf("%lf", &nota1);

    printf("Nota 2°: ");
    scanf("%lf", &nota2);

    media = (nota1 + nota2) / 2;

    puts("Média de Aproveitamento ");
    if (media >= 9 && media <= 10) {
        puts("A");
        puts("Aprovado!");
    } else if (media >= 7.5 && media < 9) {
        puts("B");
        puts("Aprovado!");
    } else if (media >= 6 && media < 7.5) {
        puts("C");
        puts("Aprovado!");
    } else if (media >= 4 && media < 6) {
        puts("D");
        puts("Reprovado!");
    } else if (media >= 0 && media < 4) {
        puts("E");
        puts("Reprovado!");
    }
}

int main() {
    conceito();
}