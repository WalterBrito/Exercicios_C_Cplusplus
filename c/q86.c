/*
Em uma eleição presidencial existem quatro candidatos. Os votos são informados
por meio de código. Os códigos utilizados são:
1 , 2, 3, 4 - Votos para os respectivos candidatos
(você deve montar a tabela ex: 1 - Jose/ 2- João/etc)
5 - Voto Nulo
6 - Voto em Branco

Faça um programa que calcule e mostre:

a. O total de votos para cada candidato;
b. O total de votos nulos;
c. O total de votos em branco;
d. A percentagem de votos nulos sobre o total de votos;
e. A percentagem de votos em branco sobre o total de votos.

Para finalizar o conjunto de votos tem-se o valor zero.
*/

#include <stdio.h>
#include <stdbool.h>

void eleicao();

int main() {
    eleicao();
}

void eleicao() {
    int voto;
    int canditato1 = 0;
    int canditato2 = 0;
    int canditato3 = 0;
    int canditato4 = 0;
    int voto_nulo = 0;
    int voto_branco = 0;
    int total = 0;
    int percentagem_nulo;
    int percentagem_branco;

    puts("\t\tTabela de Candidatos");
    printf("1 - José\n2 - João\n3 - Paulo\n4 - Pedro\n5 - Nulo\n6 - Branco\n");

    while (true) {
        printf("Digite seu voto: ");
        scanf("%d", &voto);

        if (voto == 0) {
            break;
        }

        if (voto == 1) {
            canditato1++;
        } else if (voto == 2) {
            canditato2++;
        } else if (voto == 3) {
            canditato3++;
        } else if (voto == 4) {
            canditato4++;
        } else if (voto == 5) {
            voto_nulo++;
        } else if (voto == 6) {
            voto_branco++;
        }

        total++;
    }

    printf("\nJosé: %d voto(s).", canditato1);
    printf("\nJoão: %d voto(s).", canditato2);
    printf("\nPaulo: %d voto(s).", canditato3);
    printf("\nPedro: %d voto(s).", canditato4);
    printf("\nVotos nulos: %d voto(s).", voto_nulo);
    printf("\nVotos em branco: %d voto(s).\n", voto_branco);

    percentagem_nulo = total / voto_nulo;
    printf("\nPercentagem de votos nulos: %d%%", percentagem_nulo);

    percentagem_branco = total / voto_branco;
    printf("\nPercentagem de votos em branco: %d%%\n", percentagem_branco);
}
