// Função para imprimir string em escada

#ifndef EXERCICIOS_STRING_ESCADA_H
#define EXERCICIOS_STRING_ESCADA_H

#endif //EXERCICIOS_STRING_ESCADA_H

#include <string.h>
#include <stdio.h>

void vertical_escada(char* x) {

    // Percorre o vetor
    for (int j = 0; j < strlen(x); ++j) {
        printf("%s\n", &x[j]);
    }
}