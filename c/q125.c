/*
'''Conta espaços e vogais.''' Dado uma string com uma frase informada pelo
usuário (incluindo espaços em branco), conte:
a. quantos espaços em branco existem na frase.
b. quantas vezes aparecem as vogais a, e, i, o, u.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TAMANHO 50

void contagem();

int main() {
    contagem();
}

void contagem() {

    char palavra[TAMANHO];
    char letras[5] = {'a', 'e', 'i', 'o', 'u'};

    int espaco = 0;
    int letra = 0;

    printf("Digite uma palavra: ");
    fgets(palavra, TAMANHO, stdin);

    for (int i = 0; i < strlen(palavra); ++i) {
        if (palavra[i] == ' ') {
            espaco++;
        }

        for (int j = 0; j < sizeof(letra); ++j) {
            if (palavra[i] == letras[j]) {
                letra++;
            }
        }
    }

    printf("\n%d espaços em branco.", espaco);
    printf("\nLetras 'a, e , i , o, u', aparecem %d vezes.\n", letra);

}
