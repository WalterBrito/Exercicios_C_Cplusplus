/*
'''Data com mês por extenso'''. Construa uma função que receba uma data no
formato ''DD/MM/AAAA'' e devolva uma string no formato ''D de
mesPorExtenso de AAAA''. Opcionalmente, valide a data e retorne NULL caso a
data seja inválida.
*/

#include <stdio.h>

#include "q118.h"

void data();

int main() {
    data();
}

void data() {
    int d;
    int m;
    int a;

    printf("Dia: ");
    scanf("%d", &d);

    printf("Mês: ");
    scanf("%d", &m);

    printf("Ano: ");
    scanf("%d", &a);

    printf("\n%c de %c de \n", dia(d), mes(m));

}

