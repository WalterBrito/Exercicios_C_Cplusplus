/*
Faça um Programa que leia 20 números inteiros e armazene-os num vetor.
Armazene os números pares no vetor PAR e os números IMPARES no vetor
impar. Imprima os três vetores.
*/

#include <stdio.h>

int main() {
    int par_impar[20] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
    int par[10];
    int impar[10];

    printf("Vetor: ");
    for (int k = 0; k < 20; ++k) {
        printf("%d ", par_impar[k]);
    }

    puts("");

    printf("Par: ");
    for (int i = 0; i < 20; ++i) {

        if (par_impar[i] % 2 == 0) {
            par[i] = par_impar[i];
            printf("%d ", par[i]);
        }
    }

    puts("");

    printf("Impar: ");
    for (int j = 0; j < 20; ++j) {

        if (par_impar[j] % 2 != 0) {
            impar[j] = par_impar[j];
            printf("%d ", impar[j]);
        }
    }

    puts("");
}