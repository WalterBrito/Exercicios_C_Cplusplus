/*
'''Embaralha palavra'''. Construa uma função que receba uma string como
parâmetro e devolva outra string com os carateres embaralhados. Por exemplo: se
função receber a palavra ''python'', pode retornar ''npthyo'', ''ophtyn'' ou qualquer
outra combinação possível, de forma aleatória. Padronize em sua função que
todos os caracteres serão devolvidos em caixa alta ou caixa baixa,
independentemente de como foram digitados.
*/

#include <stdio.h>
#include <stdlib.h>

#include "reverso.h"

#define TAMANHO 20


int main() {

    char nome[TAMANHO];

    printf("Digite um nome: ");
    scanf("%s", nome);

    puts("");
    reverso(nome);
    puts("");
}