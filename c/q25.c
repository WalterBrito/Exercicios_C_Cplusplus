/*
Faça um Programa que pergunte em que turno você estuda. Peça para digitar
M-matutino ou V-Vespertino ou N-Noturno. Imprima a mensagem "Bom Dia!",
"Boa Tarde!" ou "Boa Noite!" ou "Valor Inválido!", conforme o caso.
*/

#include <stdio.h>
#include <ctype.h>

void turno() {
    char horario;

    printf("\t\tHorários disponiveis\n");
    printf("\t\nM-matutino\nV-Vespertino\nN-Noturno\n");
    printf("Qual horário você estuda?: ");
    scanf("%s", &horario);

    if (horario == 'm') {
        printf("\nBom dia!\n");
    } else if (horario == 'v') {
        printf("\nBoa tarde!\n");
    } else if (horario == 'n') {
        printf("\nBoa noite!\n");
    } else {
        printf("\nInválido.\n");
    }
}

int main() {
    turno();
}