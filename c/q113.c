/*
Faça um programa que converta da notação de 24 horas para a notação de 12
horas. Por exemplo, o programa deve converter 14:25 em 2:25 P.M. A entrada é
dada em dois inteiros. Deve haver pelo menos duas funções: uma para fazer a
conversão e uma para a saída. Registre a informação A.M./P.M. como um valor
‘A’ para A.M. e ‘P’ para P.M. Assim, a função para efetuar as conversões terá um
parâmetro formal para registrar se é A.M. ou P.M. Inclua um loop que permita
que o usuário repita esse cálculo para novos valores de entrada todas as vezes que
desejar.
*/

#include <stdio.h>

#include "q113.h"

void resultado();

int main() {
    resultado();
}

void resultado() {
    short int i = 0;
    short int hora;
    short int minuto;

    while (1) {
        printf("Hora: ");
        scanf("%hi", &hora);

        if (hora == -1) {
            break;
        }

        printf("Minuto: ");
        scanf("%hi", &minuto);

        if ((hora >= 13 && hora <= 23) && (minuto > 0 && minuto < 60)) {
            printf("\n\n%d:%d P.M ~> %d:%d A.M\n\n", hora, minuto, conversao(hora), minuto);
        } else if (hora == 0) {
            printf("\n\n%d:%d P.M ~> %d:%d A.M\n\n", hora, minuto, conversao(hora), minuto);
        }

        i++;
    }
}