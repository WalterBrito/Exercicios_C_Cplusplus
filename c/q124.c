/*
'''Nome na vertical em escada invertida.''' Altere o programa anterior de modo que
a escada seja invertida.
FULANO
FULAN
FULA
FUL
FU
F
*/

#include "string_escada_invertida.h"

int main() {

    char palavra[20];

    printf("Digite uma palavra: ");
    scanf("%s", palavra);

    escada_invertida(palavra);
}