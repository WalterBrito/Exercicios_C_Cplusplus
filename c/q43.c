/*
O Hipermercado Tabajara está com uma promoção de carnes que é
imperdível. Confira:

                    Até 5 Kg            Acima de 5 Kg
File Duplo          R$ 4,90 por Kg      R$ 5,80 por Kg
Alcatra             R$ 5,90 por Kg      R$ 6,80 por Kg
Picanha             R$ 6,90 por Kg      R$ 7,80 por Kg

Para atender a todos os clientes, cada cliente poderá levar apenas um dos tipos de
carne da promoção, porém não há limites para a quantidade de carne por cliente.
Se compra for feita no cartão Tabajara o cliente receberá ainda um desconto de
5% sobre o total a compra. Escreva um programa que peça o tipo e a quantidade
de carne comprada pelo usuário e gere um cupom fiscal, contendo as informações
da compra: tipo e quantidade de carne, preço total, tipo de pagamento, valor do
desconto e valor a pagar.
*/

#include <stdio.h>
#include <string.h>

char tipo_de_carne[50];

int tipo_de_pagamento;

double valor_desconto;

double preco_file;
double preco_alcatra;
double preco_picanha;
double qtidade_carne;
double preco_total;
double valor_a_pagar;

void compras() {
    printf("Qual o tipo de carne: ");
    scanf("%[^\n]%*c", tipo_de_carne);

    printf("Quantidade de carne: ");
    scanf("%lf", &qtidade_carne);


    if (qtidade_carne <= 5) {
        preco_file = 4.90;
        preco_alcatra = 5.90;
        preco_picanha = 6.90;
    }

    if (qtidade_carne >= 5) {
        preco_file = 5.80;
        preco_alcatra = 6.80;
        preco_picanha = 7.80;
    }

    if (tipo_de_carne == (char *) 'f') {
        preco_total = preco_file * qtidade_carne;
//        tipo_de_carne = "Filé duplo";
    } else if (tipo_de_carne == (char *) 'a') {
        preco_total = preco_alcatra * qtidade_carne;
//        tipo_de_carne = "Alcatra";
    } else if (tipo_de_carne == (char *) 'p') {
        preco_total = preco_picanha * qtidade_carne;
//        tipo_de_carne = "Picanha";
    }

    puts("1 - cartão tanajara / 2 - à vista");
    printf("Forma de pagamento: ");
    scanf("%d", &tipo_de_pagamento);

    if (tipo_de_pagamento == 1) {
        puts("Cartão Tabajara");
    } else {
        puts("À vista");
    }

    valor_desconto = preco_total * 0.5;
    valor_a_pagar = preco_total - valor_desconto;

    puts("\n==================== Cupom Fiscal ====================");
    printf("\nTipo: %s", tipo_de_carne);
    printf("\n%.1lf Kg de carne", qtidade_carne);
    printf("\nPreço Total: R$ %.2lf", preco_total);
    printf("\nTipo de pagamento: %d", tipo_de_pagamento);
    printf("\nDesconto: R$ %.2lf", valor_desconto);
    printf("\nValor à pagar: R$ %.2lf\n", valor_a_pagar);
}


int main() {
    compras();
}