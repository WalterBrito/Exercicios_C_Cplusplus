/*
Altere o programa de cálculo do fatorial, permitindo ao usuário calcular o fatorial
várias vezes e limitando o fatorial a números inteiros positivos e menores que 16.
*/

#include <stdio.h>
#include <stdbool.h>

unsigned long long int fatorial(unsigned int n);
int numero;
unsigned int i = 1;

int main(void) {
    printf("Digite um número: ");
    scanf("%d", &numero);

//    for (unsigned int i = 1; i <= numero; i++) {
//
//        if (numero > 1 && numero < 16) {
//            printf("%u! = %llu\n", i, fatorial(i));
//        } else {
//            printf("\npermitido somente números interiros positivos menores que 16.\n");
//        }
//    }

    while (i <= numero) {
        if (numero > 1 && numero < 16) {
            printf("%u! = %llu\n", i, fatorial(i));
        } else {
            printf("\npermitido somente números interiros positivos menores que 16.\n");
            break;
        }
        i++;
    }
}

unsigned long long int fatorial(unsigned int n) {

    if (n <= 1) {
        return 1;
    } else {
        return (n * fatorial(n - 1));
    }
}