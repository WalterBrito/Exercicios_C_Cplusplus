/*
Faça um Programa que peça a temperatura em graus Celsius, transforme e mostre
em graus Farenheit.
F = (C *1,8)+32
*/

#include <stdio.h>
#include <stdlib.h>


void temperaturaC() {
    double farenheit;
    double celsius;

    printf("Temp. Celsius: ");
    scanf("%lf", &celsius);

    farenheit = (5 * (celsius * 1.8) + 32);

    printf("\n%.1lf C° é %.1lf F°\n", celsius, farenheit);
}

int main() {
    temperaturaC();
}