/*
Faça um programa que receba o valor de uma dívida e mostre uma tabela com os
seguintes dados: valor da dívida, valor dos juros, quantidade de parcelas e valor
da parcela.
Os juros e a quantidade de parcelas seguem a tabela abaixo:
Quantidade de Parcelas % de Juros sobre o valor inicial da dívida
1   0
3   10
6   15
9   20
12  25

Exemplo de saída do programa:
Valor da Dívida Valor dos Juros Quantidade de Parcelas Valor da Parcela

R$ 1.000,00         0                    1                R$ 1.000,00
R$ 1.100,00         6                    3                R$ 366,00
R$ 1.150,00         50                   6                R$ 191,67
*/


#include <stdio.h>

int qtidade_parcelas;

double valor_divida;
double valor_parcelas;
double valor_juros;

void divida() {
    printf("Valor da divida: ");
    scanf("%lf", &valor_divida);

    printf("Quantidade de parcelas: ");
    scanf("%d", &qtidade_parcelas);

    puts(" ");

    for (int i = 0; i < qtidade_parcelas; i++) {

        if (qtidade_parcelas >= 1 && qtidade_parcelas < 3) {
            valor_juros = 0;
        } else if (qtidade_parcelas >= 3 && qtidade_parcelas < 6) {
            valor_juros = 0.1;
        } else if (qtidade_parcelas >= 6 && qtidade_parcelas < 9) {
            valor_juros = 0.15;
        } else if (qtidade_parcelas >= 9 && qtidade_parcelas < 12) {
            valor_juros = 0.2;
        } else if (qtidade_parcelas >= 12) {
            valor_juros += 0.25;
        }
    }

    puts("Valor da divida\t Valor do Juros\t Quantidade de parcelas\t Valor da parcela");
    valor_parcelas = (valor_divida * valor_juros) / qtidade_parcelas;
    printf("\nR$ %.2lf \t\t%.1lf \t\t%d \t\t%.2f\n", valor_divida,  valor_juros,
           qtidade_parcelas, valor_parcelas);
}

int main() {
    divida();
}

