/*
Faça um Programa que peça um número e informe se o número é inteiro ou
decimal. Dica: utilize uma função de arredondamento.
*/

#include <stdio.h>
#include <math.h>

double num;
int aux;

int main() {
    printf("Digite um número: ");
    scanf("%lf", &num);

    aux = (int) num;

    if (aux == num) {
        printf("\n%d é inteiro.\n", aux);
    } else {
        printf("\n%.1lf é decimal.\n", num);
    }
}

