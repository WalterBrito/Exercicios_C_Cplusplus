/*
Faça um Programa para leitura de três notas parciais de um aluno. O programa
deve calcular a média alcançada por aluno e presentar:
a. A mensagem "Aprovado", se a média for maior ou igual a 7, com a
respectiva média alcançada;
b. A mensagem "Reprovado", se a média for menor do que 7, com a
respectiva média alcançada;
c. A mensagem "Aprovado com Distinção", se a média for igual a 10.
*/

#include <stdio.h>

double nota1;
double nota2;
double nota3;
double media;

int main() {

    printf("Nota 1°: ");
    scanf("%lf", &nota1);

    printf("Nota 2°: ");
    scanf("%lf", &nota2);

    printf("Nota 3°: ");
    scanf("%lf", &nota3);

    media = (nota1 + nota2 + nota3) / 3;
    printf("Média: %.1lf\n", media);

    if (media == 10) {
        puts("Aprovado com Distinção");
    } else if (media >= 7 && media < 10) {
        puts("Aprovado");
    } else if (media < 7) {
        puts("Reprovado");
    }
}