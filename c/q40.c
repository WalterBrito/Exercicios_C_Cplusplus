/*
Faça um programa que faça 5 perguntas para uma pessoa sobre um crime. As
perguntas são:
a. "Telefonou para a vítima?"
b. "Esteve no local do crime?"
c. "Mora perto da vítima?"
d. "Devia para a vítima?"
e. "Já trabalhou com a vítima?"
O programa deve no final emitir uma classificação sobre a participação da
pessoa no crime. Se a pessoa responder positivamente a 2 questões ela deve ser
classificada como "Suspeita", entre 3 e 4 como "Cúmplice" e 5 como "Assassino".
Caso contrário, ele será classificado como "Inocente".
*/

#include <stdio.h>
#include <string.h>

char pergunta;

int cont = 0;

void investigacao() {
    printf("Telefonou para a vítima?:");
    scanf("%s", &pergunta);

    if (pergunta == 's') {
        cont = cont + 1;
    }

    printf("Esteve no local do crime?: ");
    scanf("%s", &pergunta);

    if (pergunta == 's') {
        cont = cont + 1;
    }

    printf("Mora perto da vítima?: ");
    scanf("%s", &pergunta);

    if (pergunta == 's') {
        cont = cont + 1;
    }

    printf("Devia para a vítima?: ");
    scanf("%s", &pergunta);

    if (pergunta == 's') {
        cont = cont + 1;
    }

    printf("Já trabalhou com a vítima?: ");
    scanf("%s", &pergunta);

    if (pergunta == 's') {
        cont = cont + 1;
    }

    if (cont == 2) {
      puts("Suspeito!");
    } else if (cont == 3 || cont == 4) {
        puts("Cúmplice\n");
    } else if (cont == 5) {
        puts("Assassino\n");
    } else {
        puts("Inocente\n");
    }
}

int main() {
    investigacao();
}