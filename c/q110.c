/*
Faça um programa, com uma função que necessite de três argumentos, e que
forneça a soma desses três argumentos.
*/

#include <stdio.h>

#define TAMANHO 3

int soma(int n1, int n2, int n3);

int main() {

    int n;
    int x[TAMANHO];

    for (int i = 0; i < TAMANHO; ++i) {
        printf("Número %d: ", i + 1);
        scanf("%d", &n);

        x[i] = n;
    }

    printf("\nSoma: %d\n", soma(x[0], x[1], x[2]));
}

int soma(int n1, int n2, int n3) {

    return n1 + n2 + n3;
}