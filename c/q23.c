/*
Faça um programa que pergunte o preço de três produtos e informe qual produto
você deve comprar, sabendo que a decisão é sempre pelo mais barato.
*/

#include <stdio.h>
#include <stdlib.h>

int main() {
    double produto1;
    double produto2;
    double produto3;

    printf("Preço produto 1: ");
    scanf("%lf", &produto1);

    printf("Preço produto 2: ");
    scanf("%lf", &produto2);

    printf("Preço produto 3: ");
    scanf("%lf", &produto3);

    if (produto1 < produto2 && produto1 < produto3) {
        printf("\nProduto 1 é o mais barato!");
        printf("\nValor: R$ %.2lf\n", produto1);
    } else if (produto2 < produto3 && produto2 < produto1) {
        printf("\nProduto 2 é o mais barato!");
        printf("\nValor: R$ %.2lf\n", produto2);
    } else if (produto3 < produto1 && produto3 < produto2) {
        printf("\nProduto 3 é o mais barato!");
        printf("\nValor: R$ %.2lf\n", produto3);
    } else {
        printf("\nProduto inválido.\n");
    }
}