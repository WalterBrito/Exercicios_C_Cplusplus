/*
Faça um programa com uma função chamada somaImposto. A função possui dois
parâmetros formais: taxaImposto, que é a quantia de imposto sobre vendas
expressa em porcentagem e custo, que é o custo de um item antes do imposto. A
função “altera” o valor de custo para incluir o imposto sobre vendas.
*/

#include <stdio.h>

double somaImposto(double taxaImposto, double custo);
void imposto();

int main() {
    imposto();
}

double somaImposto(double taxaImposto, double custo) {
    double x;

    x = custo * (taxaImposto / 100);

    return x;
}

void imposto() {

    double c; // Valor do custo
    double i; // Valor do imposto
    double valor;

    printf("Digite um valor: ");
    scanf("%lf", &c);

    printf("Digite o valor do imposto (Ex.: 5): ");
    scanf("%lf", &i);

    printf("\nCusto: R$ %.2lf\n", c);
    printf("Imposto: %d%%\n", (int) i);

    printf("\nValor do imposto: %.2lf\n", somaImposto(c, i));
    printf("Valor total: %.2lf\n", (c + somaImposto(c, i)));
}
