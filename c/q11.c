/*
Faça um Programa que peça 2 números inteiros e um número real. Calcule e
mostre:
a. o produto do dobro do primeiro com metade do segundo .
b. a soma do triplo do primeiro com o terceiro.
c. o terceiro elevado ao cubo.
*/

#include <stdio.h>
#include <stdlib.h>

int num1;
int num2;
int num3;
int resul1;
int resul2;
int resul3;

void dobro() {
    printf("Número 1: ");
    scanf("%d", &num1);
}

void triplo() {
    printf("Número 2: ");
    scanf("%d", &num2);
}

void cubo() {
    printf("Número 3: ");
    scanf("%d", &num3);
}

void resultado() {
    resul1 = ((num1 * 2) * (num2 / 2));
    printf("\no produto do dobro do primeiro com metade do segundo ~> %d\n", resul1);

    resul2 = ((num1 * num1 * num1) + num3);
    printf("a soma do triplo do primeiro com o terceiro ~> %d\n", resul2);

    resul3 = (num3 * num3 * num3);
    printf("o terceiro elevado ao cubo ~> %d\n", resul3);
}

int main() {
    dobro();
    triplo();
    cubo();
    resultado();
}