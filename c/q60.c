/*
Faça um programa que calcule o fatorial de um número inteiro fornecido pelo
usuário. Ex.: 5!=5.4.3.2.1=120
*/

#include <stdio.h>

unsigned long long int fatorial(unsigned int n);
int numero;

int main(void) {
    printf("Digite um número: ");
    scanf("%d", &numero);

    for (unsigned int i = 1; i <= numero; i++) {
        printf("%u! = %llu\n", i, fatorial(i));
    }
}

unsigned long long int fatorial(unsigned int n) {

    if (n <= 1) {
        return 1;
    } else {
        return (n * fatorial(n - 1));
    }
}