/*
Faça um programa para imprimir:
1
2 2
3 3 3
.....
n n n n n n ... n

para um '''n''' informado pelo usuário. Use uma função que receba um
valor '''n''' inteiro e imprima até a n-ésima linha.
*/

#include <stdio.h>

void numero(int x);

int main() {

    int n;

    printf("Digite um número: ");
    scanf("%d", &n);

    for (int j = 1; j < n; ++j) {
        numero(j);
    }
}


void numero(int x) {

    for (int i = 1; i <= x; ++i) {
        printf("%d ", x);
    }

    puts("");
}

