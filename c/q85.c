/*
O cardápio de uma lanchonete é o seguinte:
                    Especificação Código        Preço
Cachorro Quente         100                     R$ 1,20
Bauru Simples           101                     R$ 1,30
Bauru com ovo           102                     R$ 1,50
Hambúrguer              103                     R$ 1,20
Cheeseburguer           104                     R$ 1,30
Refrigerante            105                     R$ 1,00

Faça um programa que leia o código dos itens pedidos e as quantidades
desejadas. Calcule e mostre o valor a ser pago por item (preço * quantidade) e o
total geral do pedido. Considere que o cliente deve informar quando o pedido
deve ser encerrado.
*/

#include <stdio.h>
#include <stdbool.h>

int codigo;
int quantidade;

double preco;
double total_item = 0;
double total = 0;

void lanchonete() {

    puts("Para sair digite 0 (zero).");
    while (true) {
        // Pede o código do item
        printf("Código: ");
        scanf("%d", &codigo);

        // Pede a quantidade de pedidos por item
        printf("Quantidade: ");
        scanf("%d", &quantidade);

        // Encerra o pedido
        if (quantidade == 0) {
            break;
        }

        puts(" ");

        if (codigo == 100) {
            preco = 1.20;
        }

        if (codigo == 101) {
            preco = 1.30;
        }

        if (codigo == 102) {
            preco = 1.50;
        }

        if (codigo == 103) {
            preco = 1.20;
        }

        if (codigo == 104) {
            preco = 1.30;
        }

        if (codigo == 105) {
            preco = 1.00;
        }

        total_item = (preco * quantidade);
        printf("\n%d: %d R$ %.2lf.\n\n", codigo, quantidade, total_item);

        total += total_item;
    }

    printf("\nTotal: R$ %.2lf\n", total);
}

int main() {
    lanchonete();
}