// Faça um Programa que peça dois números e imprima o maior deles.

#include <stdio.h>
#include <stdlib.h>

int main() {
    int num1;
    int num2;

    printf("1° número: ");
    scanf("%d", &num1);

    printf("2° número: ");
    scanf("%d", &num2);

    if (num1 > num2) {
        printf("\nNúmero %d é maior que o número %d.\n", num1, num2);
    } else {
        printf("\nNúmero %d é maior que o número %d.\n", num2, num1);
    }
}