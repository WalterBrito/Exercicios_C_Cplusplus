// Faça um Programa que leia 4 notas, mostre as notas e a média na tela.

#include <stdio.h>
#define SIZE 4

int main() {
    int notas[4] = {4, 8, 9, 7};
    int media;
    int aux = 0;

    for (int i = 0; i < 4; i++) {
        printf("%d° nota: %d", i+1, notas[i]);
        aux += notas[i];
        puts("");
    }

    media = aux / 4;
    printf("\nMédia: %.1lf", (double) media);

    puts("");
}