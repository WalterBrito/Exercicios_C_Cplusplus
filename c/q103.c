/*
Faça um programa que receba a temperatura média de cada mês do ano e
armazene-as em uma lista. Após isto, calcule a média anual das temperaturas e
mostre todas as temperaturas acima da média anual, e em que mês elas ocorreram
(mostrar o mês por extenso: 1 – Janeiro, 2 – Fevereiro, . . . ).
*/

#include <stdio.h>

#define TEMP_SIZE 12

int main() {
    int temperatura;
    int temp_aux[12];
    int media_anual;

    for (int i = 0; i < TEMP_SIZE; ++i) {
        printf("Temperatura %d: ", i + 1);
        scanf("%d", &temperatura);

        // Add valor da temperatura no vetor
        temp_aux[i] = temperatura;

    }

    puts("");

    for (int j = 0; j < TEMP_SIZE; ++j) {
        //
        media_anual = temp_aux[j] / TEMP_SIZE;
    }

    puts("Meses acima da temperatura média anual.");
    for (int k = 0; k < TEMP_SIZE; ++k) {
        if (temp_aux[k] > media_anual) {
            printf("%d - %d\n", k + 1, temp_aux[k]);
        }
    }

    puts("");
}