/*
Faça um programa que calcule as raízes de uma equação do segundo grau, na
forma ax2 + bx + c. O programa deverá pedir os valores de a, b e c e fazer as
consistências, informando ao usuário nas seguintes situações:
a. Se o usuário informar o valor de A igual a zero, a equação não é
do segundo grau e o programa não deve fazer pedir os demais valores,
sendo encerrado;
b. Se o delta calculado for negativo, a equação não possui raizes
reais. Informe ao usuário e encerre o programa;
c. Se o delta calculado for igual a zero a equação possui apenas uma
raiz real; informe-a ao usuário;
d. Se o delta for positivo, a equação possui duas raiz reais;
informe-as ao usuário;
*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

double a;
double b;
double c;

double delta;
double raiz;
double raiz1;
double raiz2;

void equacao() {
    printf("Digite o valor de a: ");
    scanf("%lf", &a);

    if (a == 0) {
        printf("equação não é do segundo grau");
    } else {
        printf("Digite o valor de b: ");
        scanf("%lf", &b);

        printf("Digite o valor de c: ");
        scanf("%lf", &c);

        delta = pow(b, 2) - (4 * a * c);

        if (delta < 0) {
            printf("\ndelta = %.1f a equacao nao possui raizes reais", delta);
        }

        if (delta == 0) {
            printf("\ndelta = %.1lf a equacao possui uma raiz", delta);
        }

        raiz = (-1 * b + sqrt(delta)) / (2 * a);

        printf("\nraiz da equacao = %.1lf\n", raiz);

        if (delta > 0) {
            printf("\ndelta = %.1lf a equacao possui duas raizes", delta);
            raiz1 = (-1 * b + sqrt(delta)) / (2 * a);
            raiz2 = (-1 * b - sqrt(delta)) / (2 * a);
            printf("\nraiz1 da equacao = %.1lf\n", raiz1);
            printf("\nraiz2 da equacao = %.1lf\n", raiz2);
        }
    }
}

int main() {
    equacao();
}