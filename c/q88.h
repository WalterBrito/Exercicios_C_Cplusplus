#ifndef EXERCICIOS_MAXIMO_H
#define EXERCICIOS_MAXIMO_H

#endif //EXERCICIOS_Q88_H


double melhor_salto(double x) {
    double max[5];

    for (int i = 0; i < 5; ++i) {
        if (x > max[i]) {
            return max[i];
        }
    }
}

double pior_salto(double y) {
    double max[5];

    for (int i = 0; i < 5; ++i) {
        if (y < max[i]) {
            return max[i];
        }
    }
}
