/* Faça um programa para o cálculo de uma folha de pagamento, sabendo que os
descontos são do Imposto de Renda, que depende do salário bruto (conforme
tabela abaixo) e 3% para o Sindicato e que o FGTS corresponde a 11% do Salário
Bruto, mas não é descontado (é a empresa que deposita). O Salário Líquido
corresponde ao Salário Bruto menos os descontos. O programa deverá pedir ao
usuário o valor da sua hora e a quantidade de horas trabalhadas no mês.
Desconto do IR:
* Salário Bruto até 900 (inclusive) - isento
* Salário Bruto até 1500 (inclusive) - desconto de 5%
* Salário Bruto até 2500 (inclusive) - desconto de 10%
* Salário Bruto 2500 - desconto de 20%

Imprima na tela as informações, dispostas conforme o exemplo abaixo.
No exemplo o valor da hora é 5 e a quantidade de hora é 220.
Salário Bruto: (5 * 220) : R$ 1100,00
(-) Sindicato (3%): R$ 55,00
(-) INSS ( 10%): R$ 110,00
FGTS (11%): R$ 121,00
Total de descontos: R$ 165,00
Salário Liquido: R$ 935,00
*/

#include <stdio.h>

int valor_hora;
int horas_trabalhadas;

double salario_bruto;
double salario_liquido;
double imposto_de_renda = 0.05;
double sindicato;
double fgts;
double desconto = 0;
double *descontoPtr = &desconto;
double total_descontos = 0;

void folha_de_pagamento() {
    printf("Valor da hora trabalhada: ");
    scanf("%d", &valor_hora);

    printf("Quantas horas trabalhadas?: ");
    scanf("%d", &horas_trabalhadas);

    // Valor do saláro bruto
    salario_bruto = (valor_hora * horas_trabalhadas);
    printf("\nSalário bruto: R$ %.2lf\n", salario_bruto);

    if (salario_bruto <= 900) {
        printf("Isento!\n");
    } else if (salario_bruto > 900 && salario_bruto <= 1500) {
        *descontoPtr = 0.05;
    } else if (salario_bruto > 1500 && salario_bruto <= 2500) {
        *descontoPtr = 0.1;
    } else if (salario_bruto > 2500) {
        *descontoPtr = 0.2;
    }

    // Valor descontdo do imposto de renda
    imposto_de_renda = (salario_bruto * *descontoPtr);
    printf("Imposto de renda: R$%.2lf\n", imposto_de_renda);

    // Valor descontado sindicato
    sindicato = (salario_bruto * 0.03);
    printf("Sindicato: R$%.2lf\n", sindicato);

    // Valor do FGTS
    fgts = (salario_bruto * 0.11);
    printf("FGTS: R$%.2lf\n", fgts);

    // Total dos descontos
    total_descontos = (imposto_de_renda + sindicato);
    printf("Total de descontos: R$%.2lf\n", total_descontos);

    // Salário liquido
    salario_liquido = (salario_bruto - total_descontos);
    printf("Salário Liquido: R$%.2lf\n", salario_liquido);
}

int main() {
    folha_de_pagamento();
}

