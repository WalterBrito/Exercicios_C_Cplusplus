/*
Em uma competição de salto em distância cada atleta tem direito a cinco saltos.
No final da série de saltos de cada atleta, o melhor e o pior resultados são
eliminados. O seu resultado fica sendo a média dos três valores restantes. Você
deve fazer um programa que receba o nome e as cinco distâncias alcançadas pelo
atleta em seus saltos e depois informe a média dos saltos conforme a descrição
acima informada (retirar o melhor e o pior salto e depois calcular a média). Faça
uso de uma lista para armazenar os saltos. Os saltos são informados na ordem da
execução, portanto não são ordenados. O programa deve ser encerrado quando
não for informado o nome do atleta. A saída do programa deve ser conforme o
exemplo abaixo:

Atleta: Rodrigo Curvêllo
Primeiro Salto: 6.5 m
Segundo Salto: 6.1 m
Terceiro Salto: 6.2 m
Quarto Salto: 5.4 m
Quinto Salto: 5.3 m
Melhor salto: 6.5 m
Pior salto: 5.3 m
Média dos demais saltos: 5.9 m

Resultado final:
Rodrigo Curvêllo: 5.9 m
*/

#include <stdio.h>
#include <stdbool.h>

#include "q88.h"

void salto();

int main() {
  salto();
}

void salto() {
    char nome[10];

    double s[5];
    double salto;
    double m_salto[1];
    double aux_m = 0;
    double p_salto[1];
    double aux_p = 0;
    double media_salto;

    printf("Nome: ");
    scanf("%s", nome);

    puts("");

    for (int i = 0; i < 5; i++) {
        printf("%d salto: ", i+1);
        scanf("%lf", &salto);

        s[i] = salto;
    }

    puts("");

    printf("Atleta: %s\n", nome);
    for (int j = 0; j < 5; ++j) {
        printf("%d Salto: %.2lf\n", j+1, s[j]);
    }

    puts("");

    for (int k = 0; k < 5; ++k) {

        if (salto > s[k]) {
            m_salto[k] = salto;
            aux_m = m_salto[k];
        }
    }

    printf("\nMelhor salto: %.2lf", aux_m);

    for (int l = 0; l < 5; ++l) {

        if (salto < s[l]) {
            p_salto[l] = salto;
            aux_p = p_salto[l];
        }
    }

    printf("\nPior Salto: %.2lf\n", aux_p);
}
