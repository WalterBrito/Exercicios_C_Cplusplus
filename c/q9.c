/*
Faça um Programa que peça a temperatura em graus Farenheit, transforme e
mostre a temperatura em graus Celsius.
C = (5 * (F-32) / 9).
*/

#include <stdio.h>
#include <stdlib.h>


void temperaturaF() {
    double farenheit;
    double celsius;

    printf("Temp. Farenheit: ");
    scanf("%lf", &farenheit);

    celsius = (5 * (farenheit - 32) / 9);

    printf("\n%.1lf F° é %.1lf C°\n", farenheit, celsius);
}

int main() {
    temperaturaF();
}