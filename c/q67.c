// Faça um programa que calcule o mostre a média aritmética de N notas.

#include <stdio.h>
#define SIZE 4;

double notas;
double media;
int count;

int main() {
   for (int i = 1; i <= 4; i++) {
       printf("%d° nota: ", i);
       scanf("%lf", &notas);

       count += notas;
   }

   media = count / 4;

   printf("\nMédia: %.1lf\n", media);
}