#include <stdlib.h>
#include <string.h>

#ifndef EXERCICIOS_Q115_H
#define EXERCICIOS_Q115_H

#endif //EXERCICIOS_Q115_H

int conta_digito(int x) {

    int contador = 0;

    while (x != 0) {
        // x = x/10
        x /= 10;

        contador++;
    }

    return contador;
}