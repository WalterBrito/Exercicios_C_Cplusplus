/*
Faça um programa que peça 10 números inteiros, calcule e mostre a quantidade de
números pares e a quantidade de números impares.
*/

#include <stdio.h>

int num;
int count_par;
int count_impar;

int main() {

    for (int i = 0; i < 10; i++) {

        printf("Digite um número: ");
        scanf("%d", &num);

        if (num % 2 == 0) {
            count_par++;
        } else if (num % 2 != 0) {
            count_impar++;
        }
    }

    printf("\nPares: %d", count_par);
    printf("\nImpares: %d\n", count_impar);
}