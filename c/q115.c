/*
Faça uma função que informe a quantidade de dígitos de um determinado número
inteiro informado.
*/

#include <stdio.h>

#include "q115.h"

void quantidade_digitos();

int main() {
    quantidade_digitos();
}

void quantidade_digitos() {
    int digito;

    printf("Digito (Ex.: 123): ");
    scanf("%d", &digito);

    printf("\nQuantidade: %d\n", conta_digito(digito));
}