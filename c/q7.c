/*
Faça um Programa que calcule a área de um quadrado, em seguida mostre o dobro
desta área para o usuário.
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void area_quadrado() {
    int area;
    int area_dobro;

    printf("Digite um valor: ");
    scanf("%d", &area);

    area_dobro = (area * area);
    printf("O dobro de area de %d é %d\n", area, area_dobro);

}

int main() {
    area_quadrado();
}
