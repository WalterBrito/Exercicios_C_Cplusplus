/*
'''Tamanho de strings.''' Faça um programa que leia 2 strings e informe o conteúdo
delas seguido do seu comprimento. Informe também se as duas strings possuem o
mesmo comprimento e são iguais ou diferentes no conteúdo.

Compara duas strings
String 1: Brasil Hexa 2006
String 2: Brasil! Hexa 2006!
Tamanho de "Brasil Hexa 2006": 16 caracteres
Tamanho de "Brasil! Hexa 2006!": 18 caracteres
As duas strings são de tamanhos diferentes.
As duas strings possuem conteúdo diferente.
*/

#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define TAMANHO 20

void le_nome();

int main() {
    le_nome();
}

void le_nome() {
    char nome1[TAMANHO];
    char nome2[TAMANHO];

    int cont1 = 0;
    int cont2 = 0;

    printf("1° palavra: ");
    scanf("%s", nome1);

    printf("2° palavra: ");
    scanf("%s", nome2);

    for (int i = 0; i < strlen(nome1); ++i) {
        cont1++;
    }

    for (int j = 0; j < strlen(nome2); ++j) {
        cont2++;
    }

    printf("\nString 1: %s", nome1);
    printf("\nString 2: %s", nome2);
    printf("\nTamanho de \"%s\": %d caractereres", nome1, cont1);
    printf("\nTamanho de \"%s\": %d caractereres", nome2, cont2);
    printf("\nTamanho: %d. As duas strings são de tamanhos diferentes.", strcmp(nome1, nome2));

    if (strlen(nome1) != strlen(nome2)) {
        printf("\nAs duas strings possuem conteúdo diferente.\n");
    }
}