/*
Supondo que a população de um país A seja da ordem de 80000 habitantes com
uma taxa anual de crescimento de 3% e que a população de B seja 200000
habitantes com uma taxa de crescimento de 1.5%. Faça um programa que calcule
e escreva o número de anos necessários para que a população do país A ultrapasse
ou iguale a população do país B, mantidas as taxas de crescimento.
*/

#include <stdio.h>

int paisA = 80000;
int paisB = 200000;

double taxa_paisA = 0.03;
double taxa_paisB = 0.015;

double anos_necessariosA;
double anos_taxaB;

int main() {

    anos_necessariosA = paisA * taxa_paisA;
    anos_taxaB = paisB * taxa_paisB;

    do   {
        printf("\nPais A ultrapassará ou igualará pais B em %d anos.\n", (int) anos_necessariosA);
    } while (anos_necessariosA >= anos_taxaB;
}