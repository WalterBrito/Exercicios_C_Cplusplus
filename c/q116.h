#ifndef EXERCICIOS_Q116_H
#define EXERCICIOS_Q116_H

#endif //EXERCICIOS_Q116_H

int reverso(int x) {
    int y = 0;
    int z = 0; // Retorna o reverso

    while (x > 0) {
        y = x % 10;
        z = z * 10 + y;
        x /= 10;
    }

    return z;
}