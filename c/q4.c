// Faça um Programa que peça as 4 notas bimestrais e mostre a média.

#include <stdio.h>
#include <stdlib.h>

void notas() {
  double nota1;
  double nota2;
  double nota3;
  double nota4;
  double media;

  printf("1° nota: ");
  scanf("%lf", &nota1);
  printf("2° nota: ");
  scanf("%lf", &nota2);
  printf("3° nota: ");
  scanf("%lf", &nota3);
  printf("4° nota: ");
  scanf("%lf", &nota4);

  media = (nota1 + nota2 + nota3 + nota4) / 4;
  printf("Média: %.1lf\n", media);
}

int main() {
  notas();
}
