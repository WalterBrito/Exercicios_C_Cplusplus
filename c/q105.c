/*
Utilize uma lista para resolver o problema a seguir. Uma empresa paga seus
vendedores com base em comissões. O vendedor recebe $200 por semana mais 9
por cento de suas vendas brutas daquela semana. Por exemplo, um vendedor que
teve vendas brutas de $3000 em uma semana recebe $200 mais 9 por cento de
$3000, ou seja, um total de $470. Escreva um programa (usando um array de
contadores) que determine quantos vendedores receberam salários nos seguintes
intervalos de valores:

a. $200 - $299
b. $300 - $399
c. $400 - $499
d. $500 - $599
e. $600 - $699
f. $700 - $799
g. $800 - $899
e. $900 - $999
h. $1000 em diante

Desafio: Crie ma fórmula para chegar na posição da lista a partir do
salário, sem fazer vários ''ifs'' aninhados.
*/

#include <stdio.h>
#include <stdbool.h>

void salario();

int main() {
    salario();
}

void salario() {
    int comissao = 200;
    int salario_vend[15];
    int venda;
    int cont = 0;
    int i = 0;
    int c200_299 = 0;
    int c300_399 = 0;
    int c400_499 = 0;
    int c500_599 = 0;
    int c700_799 = 0;
    int c600_699 = 0;
    int c800_899 = 0;
    int c900_999 = 0;
    int c1000 = 0;
    int porc = 9;
    int salario;


    while (true) {

        printf("Valor de vendas: ");
        scanf("%d", &venda);

        if (venda == 0) {
            break;
        }


        if (salario_vend[i] >= 200 && salario_vend[i] <= 299) {
             c200_299++;
        }

        if (salario_vend[i] >= 300 && salario_vend[i] <= 399) {
            c300_399++;
        }

        if (salario_vend[i] >= 400 && salario_vend[i] <= 499) {
            c400_499++;
        }

        if (salario_vend[i] >= 500 && salario_vend[i] <= 599) {
            c500_599++;
        }

        if (salario_vend[i] >= 600 && salario_vend[i] <= 699) {
            c600_699++;
        }

        if (salario_vend[i] >= 700 && salario_vend[i] <= 799) {
            c700_799++;
        }

        if (salario_vend[i] >= 800 && salario_vend[i] <= 899) {
            c800_899++;
        }

        if (salario_vend[i] >= 900 && salario_vend[i] <= 999) {
            c900_999++;
        }

        if (salario_vend[i] >= 1000) {
            c1000++;
        }

        i++;
    }

    printf("\nEntre 200 e 299: %d", c200_299);
    printf("\nEntre 300 e 399: %d", c300_399);
    printf("\nEntre 400 e 499: %d", c400_499);
    printf("\nEntre 500 e 599: %d", c500_599);
    printf("\nEntre 600 e 699: %d", c600_699);
    printf("\nEntre 700 e 799: %d", c700_799);
    printf("\nEntre 800 e 899: %d", c800_899);
    printf("\nEntre 900 e 999: %d", c900_999);
    printf("\nMaior que 1000: %d\n", c1000);

    puts("");

    salario = (comissao + porc) * venda / 100;
    printf("Valor Total: %d\n", salario);
}