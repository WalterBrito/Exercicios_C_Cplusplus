/*
'''Nome ao contrário em maiúsculas.''' Faça um programa que permita ao usuário
digitar o seu nome e em seguida mostre o nome do usuário de trás para frente
utilizando somente letras maiúsculas. Dica: lembre−se que ao informar o nome o
usuário pode digitar letras maiúsculas ou minúsculas.
*/

#include <stdio.h>
#include <string.h>

#include "reverso.h"
#include "uppercase.h"

#define TAMANHO 20


int main() {
    char palavra[TAMANHO];

    printf("Digite uma palavra: ");
    scanf("%s", palavra);

    printf("\nPalavra: %s", palavra);
    printf("\nPalavra Reverso: ");
    converteToUppercase(palavra);
    reverso(palavra);
    puts("");
}


