// Faça um Programa que converta metros para centímetros.

#include <stdio.h>
#include <stdlib.h>

void conversao() {
  int metros;
  int cm;

  printf("\tConversão metros para centimetros.\n");
  printf("Digite o valor (Ex: 12): ");
  scanf("%d", &metros);

  cm = metros * 100;
  printf("%d metros ~> %d cm\n", metros, cm);
}

int main() {
  conversao();
}
