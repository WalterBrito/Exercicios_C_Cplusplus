// Faça um Programa que leia três números e mostre o maior e o menor deles.

#include <stdio.h>

int main() {
    int num1;
    int num2;
    int num3;

    printf("Número 1°: ");
    scanf("%d", &num1);

    printf("Número 2°: ");
    scanf("%d", &num2);

    printf("Número 3°: ");
    scanf("%d", &num3);

    if (num1 > num2 && num1 > num3) {
        printf("\nO maior é %d.", num1);
    } else if (num2 > num3 && num2 > num1) {
        printf("\nO maior é %d.", num2);
    } else if (num3 > num1 && num3 > num2) {
        printf("\nO maior é %d.", num3);
    }

    if (num1 < num2 && num1 < num3) {
        printf("\nO menor é %d.\n", num1);
    } else if (num2 < num3 && num2 < num1) {
        printf("\nO menor é %d.\n", num2);
    } else if (num3 < num1 && num3 < num2) {
        printf("\nO menor é %d.\n", num3);
    }
}

