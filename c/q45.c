/*
Faça um programa que leia um nome de usuário e a sua senha e não aceite a senha
igual ao nome do usuário, mostrando uma mensagem de erro e voltando a pedir as
informações.
*/

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

char nome[30];
char senha[30];

int main() {

    do {
        printf("Digite seu nome: ");
        scanf("%[^\n]%*c", nome);

        printf("Digite sua senha: ");
        scanf("%[^\n]%*c", senha);

        puts("Nome e senha iguais.");
        printf("\n%s: %s\n", nome, senha);
    } while (!strcmp(nome, senha));
}