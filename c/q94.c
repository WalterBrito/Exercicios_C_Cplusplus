/*
Faça um Programa que leia um vetor de 10 caracteres, e diga quantas consoantes
foram lidas. Imprima as consoantes.
*/

#include <stdio.h>

int main() {
    char caracteres[10] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'};

    // Variável auxiliar para contar as consoantes
    int aux = 0;

    for (int i = 0; i < 10; i++) {
        if ((caracteres[i] != 'a') && (caracteres[i] != 'e') && (caracteres[i] != 'i')
               && (caracteres[i] != 'o') && (caracteres[i] != 'u')) {
            aux++;
            printf("%c ", caracteres[i]);
        }
    }

    printf("\n%d Consoantes.\n", aux);

}