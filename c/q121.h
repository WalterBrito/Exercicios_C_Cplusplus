#include <stdio.h>

#ifndef EXERCICIOS_Q121_H
#define EXERCICIOS_Q121_H

#endif //EXERCICIOS_Q121_H


void reverso(const char * const sPtr) {

	if ('\0' == sPtr[0]) {
		return;
	} else {
		reverso(&sPtr[1]);
		putchar(sPtr[0]);
	}
}

