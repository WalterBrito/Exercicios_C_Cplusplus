// Faça um Programa que leia um vetor de 5 números inteiros e mostre-os.

#include <stdio.h>

int main() {
    int vetor[5] = {1, 2, 3, 4, 5};

    printf("[");
    for (int i = 0; i < 5; i++) {
        printf(" %d ", vetor[i]);
    }
    printf("]\n");
}