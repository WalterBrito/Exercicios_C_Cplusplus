// Faça um Programa que verifique se uma letra digitada é vogal ou consoante.

#include <stdio.h>
#include <stdlib.h>

int main() {
    char letra;

    printf("Digite uma letra: ");
    scanf("%c", &letra);
    fflush(stdin);

    if (letra == 'a' || letra == 'e' || letra == 'i' || letra == 'o' || letra == 'u') {
        printf("\n%c é vogal!\n", letra);
    } else {
        printf("\n%c é consoante!\n", letra);
    }

    return 0;
}