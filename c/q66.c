/*
Faça um programa que mostre todos os primos entre 1 e N sendo N um número
inteiro fornecido pelo usuário. O programa deverá mostrar também o número de
divisões que ele executou para encontrar os números primos. Serão avaliados o
funcionamento, o estilo e o número de testes (divisões) executados.*
*/

#include <stdio.h>
#include <stdbool.h>

int i;
int numero;
int cont;
int x;

int main() {
    printf("Digite um número: ");
    scanf("%d", &numero);

    for (i = 1; i < numero; i++) {
        if (numero % i == 0) {
            cont++;
            x++;
        }
    }

    if (cont <= 2) {
        printf("\nO número %d é primo.\n", numero);
    } else {
        printf("O número %d não é primo.\n", numero);
    }

    numero--;
    cont = 0;

    printf("\nO número de divisões foi %d.\n", x);
}