/*
Faça um Programa que leia dois vetores com 10 elementos cada. Gere um terceiro
vetor de 20 elementos, cujos valores deverão ser compostos pelos elementos
intercalados dos dois outros vetores.
*/

#include <stdio.h>

#define SIZE 20

int main() {
    int vetor1[10] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int vetor2[10] = {11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
    int vetor3[SIZE];

    for (int i = 0; i < SIZE; ++i) {
        vetor3[i] = vetor1[i], vetor2[i];
    }

//    for (int j = 0; j < 10; ++j) {
//        vetor3[j] = vetor2[j];
//    }

    for (int k = 0; k < SIZE; ++k) {
        printf("%d ", vetor3[k]);
    }

    puts("");
}