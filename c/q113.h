#ifndef EXERCICIOS_CONVERSAO_H
#define EXERCICIOS_CONVERSAO_H

#endif //EXERCICIOS_CONVERSAO_H

int conversao(int x) {

    short int z = 0;

    switch (x) {
        case 13:
            z = 1;
            break;
        case 14:
            z = 2;
            break;
        case 15:
            z = 3;
            break;
        case 16:
            z = 4;
            break;
        case 17:
            z = 5;
            break;
        case 18:
            z = 6;
            break;
        case 19:
            z = 7;
            break;
        case 20:
            z = 8;
            break;
        case 21:
            z = 9;
            break;
        case 22:
            z = 10;
            break;
        case 23:
            z = 11;
            break;
        default:
            z = 12;
            break;
    }

    return z;
}