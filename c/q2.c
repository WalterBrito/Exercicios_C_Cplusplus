/*
Faça um Programa que peça um número e então mostre a mensagem ''O número
informado foi [número]''.
*/

#include <stdio.h>
#include <stdlib.h>

void numero() {
  int numero;

  printf("Digite um número: ");
  scanf("%d", &numero);

  printf("O número informado foi %d\n", numero);
}

int main() {
  numero();
}
