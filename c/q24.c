// Faça um Programa que leia três números e mostre-os em ordem decrescente.

#include <stdio.h>
#include <stdlib.h>

int main() {
    int num1;
    int num2;
    int num3;

    printf("Número 1°: ");
    scanf("%d", &num1);

    printf("Número 2°: ");
    scanf("%d", &num2);

    printf("Número 3°: ");
    scanf("%d", &num3);

    printf("\n\tOrdem decrescente");
    if ((num1 > num2 && num1 > num3) || (num2 > num3 && num2 > num1)
        || (num3 > num1 && num3 > num2)) {
        printf("\n%d\n%d\n%d\n", num3, num2, num1);
    }
}