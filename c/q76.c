/*
O Departamento Estadual de Meteorologia lhe contratou para desenvolver um
programa que leia as um conjunto indeterminado de temperaturas, e informe ao
final a menor e a maior temperaturas informadas, bem como a média das
temperaturas.
*/

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

int temperatura;
int temp_maior;
int temp_menor;
int temp_media;
int aux;
int media_aux = 0;
int temp_qtidade = 0;

void temp() {

    while (true) {

        printf("Digite a temperatura: ");
        scanf("%d", &temperatura);

        temp_qtidade++;

        if (temperatura < 0) {
            break;
        }

        aux = temperatura;

        if (aux > temp_maior) {
            temp_maior = aux;
        }

        if (aux < temp_menor) {
            temp_menor = aux;
        }

        media_aux += temperatura;
    }


    temp_media = media_aux / temp_qtidade;
    printf("\nMaior temperatura: %d", temp_maior);
    printf("\nMenor temperatura: %d", temp_menor);
    printf("\nMédia: %.1lf\n", (double) temp_media);
}

int main() {

    temp();
}
