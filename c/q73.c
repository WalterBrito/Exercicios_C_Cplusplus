/*
O Sr. Manoel Joaquim acaba de adquirir uma panificadora e pretende implantar a
metodologia da tabelinha, que já é um sucesso na sua loja de 1,99. Você foi
contratado para desenvolver o programa que monta a tabela de preços de pães, de
1 até 50 pães, a partir do preço do pão informado pelo usuário, conforme o
exemplo abaixo:
Preço do pão: R$ 0.18
Panificadora Pão de Ontem - Tabela de preços
1 - R$ 0.18
2 - R$ 0.36
...
50 - R$ 9.00
*/

#include <stdio.h>

int i;

double preco_do_pao;
double valor_aux = 0;

int main() {
    printf("Preço do pão: ");
    scanf("%lf", &preco_do_pao);

    puts("\n\t\tTabela de preços de pães");
    for (i = 1; i <= 50; i++) {
        valor_aux += preco_do_pao;

        printf("\n%d - %.2lf", i, valor_aux);
    }

    puts("");
}