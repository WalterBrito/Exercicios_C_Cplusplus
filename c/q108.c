/*
Faça um programa que carregue uma lista com os modelos de cinco carros
(exemplo de modelos: FUSCA, GOL, VECTRA etc). Carregue uma outra lista
com o consumo desses carros, isto é, quantos quilômetros cada um desses carros
faz com um litro de combustível. Calcule e mostre:

a. O modelo do carro mais econômico;
a. Quantos litros de combustível cada um dos carros cadastrados
consome para percorrer uma distância de 1000 quilômetros e quanto isto
custará, considerando um que a gasolina custe R$ 2,25 o litro.

Abaixo segue uma tela de exemplo. O disposição das informações deve
ser o mais próxima possível ao exemplo. Os dados são fictícios e podem
mudar a cada execução do programa.

Comparativo de Consumo de Combustível

Veículo 1
Nome: fusca
Km por litro: 7
Veículo 2
Nome: gol
Km por litro: 10
Veículo 3
Nome: uno
Km por litro: 12.5
Veículo 4
Nome: Vectra
Km por litro: 9
Veículo 5
Nome: Peugeout
Km por litro: 14.5

Relatório Final
1 - fusca    - 7.0  - 142.9  litros  - R$ 321.43
2 - gol      - 10.0 - 100.0  litros  - R$ 225.00
3 - uno      - 12.5 - 80.0   litros  - R$ 180.00
4 - vectra   - 9.0  - 111.1  litros  - R$ 250.00
5 - peugeout - 14.5 - 69.0   litros  - R$ 155.17
O menor consumo é do peugeout.
*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define TAMANHO_DO_VETOR 5
#define TAMANHO_DA_STRING 50

// Declaração de função
char capitalize(char x);

void carros();

double economico(char menor[TAMANHO_DO_VETOR]);

// Variavéis do tipo char
char modelos[TAMANHO_DA_STRING];
char model_aux[TAMANHO_DO_VETOR][TAMANHO_DA_STRING] = {{"Fusca"}, {"Gol"}, {"Palio"}, {"Siena"}, {"Peugeot"}};

int x = 1;

// Variavéis do tipo double
double consumo;
double consumoPtr[TAMANHO_DO_VETOR];
double mais_economico;
double km_por_litro;
double gasolina_litro = 2.25;
double preco_total = 0;

int main() {
    carros();
}

void carros() {

    for (int i = 0; i < TAMANHO_DO_VETOR; ++i) {
//        printf("Modelo %d: ", i + 1);
//        scanf("%s", modelos);

        // Add modelo no vetor
//        model_aux[i][TAMANHO_DA_STRING] = modelos;

        printf("Consumo modelo %d: ", i + 1);
        scanf("%lf", &consumo);

        // Add consumo no vetor
        consumoPtr[i] = consumo;

        puts("");
    }

    for (int j = 0; j < TAMANHO_DO_VETOR; ++j) {
        km_por_litro = consumoPtr[j] * 1000;
        preco_total = gasolina_litro * consumoPtr[j];
    }

    puts("\nComparativo de Consumo de Combustível");
    for (int k = 0; k < TAMANHO_DO_VETOR; ++k) {
        printf("\nVeiculo %d\n", k + 1);

        printf("Nome: %s\n", model_aux[k]);

        printf("Km por litro: %.1lf\n", consumoPtr[k]);
    }

    puts("\nRelatório Final");
    for (int l = 0; l < TAMANHO_DO_VETOR; ++l) {
        printf("%d -\t%s\t - %.1lf - %.1lf litros - R$ %.2lf\n", l + 1, model_aux[l],
               consumoPtr[l], km_por_litro, preco_total);
    }

    for (int m = 0; m < consumoPtr[TAMANHO_DO_VETOR]; ++m) {
        mais_economico = economico(model_aux[m]);
    }

    printf("\nO menor consumo é do %s\n", mais_economico);

}

// Primeira letra maiuscula
//char capitalize(char x) {
//    char y[10];
//
//    for (int i = 0; i < y[i]; ++i) {
//        y[i] = (char) toupper(y[x]);
//    }
//
//    return y[x];
//}

// Carro mais economico
double economico(char menor[TAMANHO_DO_VETOR]) {

    for (int i = 0; i < menor[TAMANHO_DO_VETOR]; ++i) {
        if (menor[i] < menor[TAMANHO_DO_VETOR]) {
            return menor[i];
        }
    }
}







