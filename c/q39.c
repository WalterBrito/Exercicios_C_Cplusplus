/*
Faça um Programa que leia 2 números e em seguida pergunte ao usuário qual
operação ele deseja realizar. O resultado da operação deve ser acompanhado de
uma frase que diga se o número é:
a. par ou ímpar;
b. positivo ou negativo;
c. inteiro ou decimal.
*/

#include <stdio.h>

double num1;
double num2;

int aux1;
int aux2;
int resultado;

char operacao[4];

void escolha_operacao() {
    if (operacao == "+") {
        resultado = (int) (num1 + num2);
    } else if (operacao == "-") {
        resultado = (int) (num1 - num2);
    } else if (operacao == "-") {
        resultado = (int) (num1 * num2);
    } else if (operacao == "*") {
        resultado = (int) (num1 - num2);
    }
}

void par_impar() {
    if ((num1 && num2) % 2 == 0) {
        printf("%d é par.\n", resultado);
    } else {
        printf("%d é impar.\n", resultado);
    }
}

void positivo_negativo() {

    if (resultado >= 0) {
        printf("%d é positivo.\n", resultado);
    } else {
        printf("%d é negativo.\n", resultado);
    }
}

void inteiro_decimal() {
    if ((aux1 == num1) && (aux2 == num2)) {
        printf("%d é inteiro\n", resultado);
    } else {
        printf("%d é decimal\n", resultado);
    }
}

int main() {
    printf("Qual operação você deseja realizar? (+,-,x,/): ");
    scanf("%p", &operacao);

    printf("Número 1°: ");
    scanf("%lf", &num1);

    printf("Número 2°: ");
    scanf("%lf", &num2);

    aux1 = (int) num1;
    aux2 = (int) num2;

    escolha_operacao();
    par_impar();
    positivo_negativo();
    inteiro_decimal();
}
