/*
Faça um Programa que peça os 3 lados de um triângulo. O programa deverá
informar se os valores podem ser um triângulo. Indique, caso os lados formem um
triângulo, se o mesmo é: equilátero, isósceles ou escaleno.
Dicas:
a. Três lados formam um triângulo quando a soma de quaisquer dois
lados for maior que o terceiro;
b. Triângulo Equilátero: três lados iguais;
c. Triângulo Isósceles: quaisquer dois lados iguais;
d. Triângulo Escaleno: três lados diferentes;
*/

#include <stdio.h>

int lado1;
int lado2;
int lado3;

void triangulo() {
    printf("1° lado: ");
    scanf("%d", &lado1);

    printf("2° lado: ");
    scanf("%d", &lado2);

    printf("3° lado: ");
    scanf("%d", &lado3);

    if ((lado1 + lado2 > lado3) || (lado1 + lado3 > lado2)
            || (lado2 + lado3 > lado1)) {
        if (lado1 == lado2 || lado1 == lado3) {
            puts("Triângulo Equilátero");
        } else if (lado1 == lado2 || lado1 == lado3 ||
                   lado2 == lado3) {
            puts("Triângulo Isósceles");
        } else if (lado1 != (lado2 && lado3) || lado2 != (lado1 && lado3)
                || lado1 != lado3) {
            puts("Triângulo Escaleno.");
        }
    } else {
        puts("Não forma um triângulo.");
    }
}

int main() {
    triangulo();
}