// Faça um Programa que peça dois números e imprima a soma.
#include <stdio.h>
#include <stdlib.h>

int main() {
  int num1;
  int num2;

  printf("Primeiro número: ");
  scanf("%d", &num1);

  printf("Segundo número: ");
  scanf("%d", &num2);

  printf("Soma: %d\n", num1 + num2);
}
