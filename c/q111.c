/*
Faça um programa, com uma função que necessite de um argumento. A função
retorna o valor de caractere ‘P’, se seu argumento for positivo, e ‘N’, se seu
argumento for zero ou negativo.
*/

#include <stdio.h>
#include <string.h>

#define TAMANHO 20

int func_caractere(int n);

void caractere ();

int main() {
    caractere();
}

int func_caractere(int n) {

    if (n > 0) {
        printf("P\n");
    } else {
        printf("N\n");
    }

    return n;
}

void caractere () {
    int num;

    printf("Digite uma número: ");
    scanf("%d", &num);

    printf("%d\n", func_caractere(num));
}
