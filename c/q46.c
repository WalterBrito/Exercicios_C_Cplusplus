/*
Faça um programa que leia e valide as seguintes informações:

a. Nome: maior que 3 caracteres;
b. Idade: entre 0 e 150;
c. Salário: maior que zero;
d. Sexo: 'f' ou 'm';
e. Estado Civil: 's', 'c', 'v', 'd';*
*/

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

char nome[30];
char sexo;
char estado_civil;

int idade;

double salario;

int main() {

    while (true) {

        printf("Digite um nome: ");
        scanf("%s", nome);

        if (strlen(nome) > 3) {
            break;
        }
    }

    while (true) {
        printf("Digite uma idade: ");
        scanf("%d", &idade);

        if (idade >= 0 && idade <= 150) {
            break;
        }
    }

    while (true) {
        printf("Digite o sexo: ");
        scanf("\n%c", &sexo);

        if (sexo == 'm' || sexo == 'f') {
            break;
        }
    }

    while (true) {
        printf("Digite o estado civil: ");
        scanf("\n%c", &estado_civil);

        if (estado_civil == 's' || estado_civil == 'c'
            || estado_civil== 'v' || estado_civil ==  'd') {
            break;
        }
    }

    while (true) {
        printf("Digite o valor do salário: ");
        scanf("%lf", &salario);

        if (salario >= 0) {
            break;
        }
    }

    printf("\nNome: %s", nome);
    printf("\nIdade: %d", idade);
    printf("\nSexo: %c", sexo);
    printf("\nEstado Civil: %c", estado_civil);
    printf("\nSalário: %.2lf\n", salario);
}




