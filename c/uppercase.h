#ifndef EXERCICIOS_UPPERCASE_H
#define EXERCICIOS_UPPERCASE_H

#endif //EXERCICIOS_UPPERCASE_H

#include <stdio.h>
#include <ctype.h>

void converteToUppercase(char *sPtr) {

	while(*sPtr != '\0') {
		*sPtr = toupper(*sPtr);
		++sPtr;
	} 
}