/*
Faça um programa que peça um numero inteiro positivo e em seguida mostre este
numero invertido.
Exemplo:
12376489
=> 98467321
*/

#include <stdio.h>
#include <stdbool.h>

int main() {
    int num;
    int x = 10;

    printf("Digite um número: ");
    scanf("%d", &num);

    for (int i = num; 0 < i; i--) {
        printf(" %d ", i);
    }
}





