/*
Foi feita uma estatística em cinco cidades brasileiras para coletar dados sobre
acidentes de trânsito. Foram obtidos os seguintes dados:
a. Código da cidade;
b. Número de veículos de passeio (em 1999);
c. Número de acidentes de trânsito com vítimas (em 1999).

Deseja-se saber:
d. Qual o maior e menor índice de acidentes de transito e a que
cidade pertence;
e. Qual a média de veículos nas cinco cidades juntas;
f. Qual a média de acidentes de trânsito nas cidades com menos de
2.000 veículos de passeio.
*/

#include <stdio.h>

#include "q82.h"

int cod_cidade;
int qtidade_veiculo;
int num_acidentes;
int media_veiculos;
int media_num_acidentes;
int aux_qtidade_veiculos = 0;
int aux_num_acidentes = 0;

void estatistica() {

    for (int i = 0; i < 5; i++) {
        printf("Código: ");
        scanf("%d", &cod_cidade);

        printf("Quantidade de carros: ");
        scanf("%d", &qtidade_veiculo);

        printf("Número de acidentes: ");
        scanf("%d", &num_acidentes);

        puts("");

        aux_qtidade_veiculos += qtidade_veiculo; // incrementa com quantidade de veiculos
        aux_num_acidentes += num_acidentes; // incrementa com quantidade de acidentes
    }

    printf("\nMaior indice: %d", maior(num_acidentes));
    printf("\nMenor indice: %d", menor(num_acidentes));

    media_veiculos = aux_qtidade_veiculos / 5;
    media_num_acidentes = aux_num_acidentes * aux_qtidade_veiculos;

    if (media_num_acidentes < 2000) {
        printf("\nMédia de acidentes com menos de 2000: %.2lf\n", (double) media_num_acidentes);
    }
}

int main() {
    estatistica();
}