/*
Faça um Programa que leia um vetor de 10 números reais e mostre-os na ordem
inversa.
*/

#include <stdio.h>

int main() {
    int vetor[10] = {1,2,3,4,5,6,7,8,9,10};

    for (int i = 10; 0 <= i; i--) {
        printf(" %d ", vetor[i]);
    }

    puts("");
}