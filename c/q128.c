/*
'''Número por extenso.''' Escreva um programa que solicite ao usuário a digitação
de um número até 999 e imprima-o na tela por extenso.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void numero_por_extenso();

int main(int argc, char *argv[]) {
    numero_por_extenso();
}

static const char * centenas[]  = { 
    "", 
    "Cento", 
    "Duzentos", 
    "Trezentos", 
    "Quatrocentos", 
    "Quinhentos", 
    "Seiscentos", 
    "Setecentos", 
    "Oitocentos", 
    "Novecentos" };

static const char *dez_vinte[] = {
        "",
        "Dez",
        "Vinte",
        "Trinta",
        "Quarenta",
        "Cinquenta",
        "Sessenta",
        "Setenta",
        "Oitenta",
        "Noventa"
};

static const char *dezenas[] = {
        "",
        "Onze",
        "Doze",
        "Treze",
        "Quatorze",
        "Quinze",
        "Dezesseis",
        "Dezesete",
        "Dezoito",
        "Dezenove"
};

static const char *const unidades[] = {
        "Zero",
        "Um",
        "Dois",
        "Três",
        "Quatro",
        "Cinco",
        "Seis",
        "Sete",
        "Oito",
        "Nove"
};

char *strcatb(char *dst, const char *src) {
    size_t len = strlen(src);
    memmove(dst + len, dst, strlen(dst) + 1);
    memcpy(dst, src, len);
    return dst;
}

char *traduzir_numero(char *nome, int n) {
    int c = n / 100;
    int d = n / 10 - c * 10;
    int u = n - (n / 10) * 10;
    int dv = d * 10 + u;

    strcpy(nome, unidades[u]);

    if (n < 10) {
        return nome;
    }

    if ((dv > 10) && (dv < 20)) {
        strcpy(nome, dez_vinte[dv - 10]);
    } else {
        if (u == 0) {
            strcpy(nome, dezenas[d]);
        } else {
            strcatb(nome, " e ");
            strcatb(nome, dezenas[d]);
        }
    }

    if (n < 100) {
        return nome;
    }

    if ((d == 0) && (u == 0)) {
        if (c == 1) {
            strcpy(nome, "Cem");
        } else {
            strcpy(nome, centenas[c]);
        } 
    } else {
        strcatb(nome, " e ");
        strcatb(nome, centenas[c]);
    }

    return nome;
}

void numero_por_extenso() {

    char extenso[100] = {0};
    int num;

    printf("Digite um número: ");
    scanf("%d", &num);

    traduzir_numero(extenso, num);

    printf("%d: %s\n", num, extenso);
}



