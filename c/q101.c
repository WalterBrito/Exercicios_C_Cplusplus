// Altere o programa anterior, intercalando 3 vetores de 10 elementos cada.

#include <stdio.h>

#define SIZE 30

int main() {
    int vetor1[10] =  {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    int vetor2[10] = {11, 12, 13, 14, 15, 16, 17, 18, 19, 20};
    int vetor3[10] = {21, 22, 23, 24, 25, 26, 27, 28, 29, 30};
    int vetor4[SIZE];

    for (int i = 0; i < vetor4[SIZE]; ++i) {
        // Inclui os 3 vetores no vetor4
        vetor4[i] = vetor1[i], vetor2[i], vetor3[i];
    }

    for (int j = 0; j < SIZE; ++j) {
        printf("%d ", vetor4[j]);
    }

    puts("");
}
