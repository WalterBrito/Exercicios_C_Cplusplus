/*
A série de Fibonacci é formada pela seqüência 0,1,1,2,3,5,8,13,21,34,55,... Faça
um programa que gere a série até que o valor seja maior que 500.
*/

#include <stdio.h>

int main() {
    int a;
    int b;
    int aux;

    a = 0;
    b = 1;

    printf("\n\tSérie de Fibonacci:\n");
    printf("%d, ", b);

    for (int i = 0; i < 500; i++) {
        aux = a + b;
        a = b;
        b = aux;

        printf("%d, ", aux);
    }
}



