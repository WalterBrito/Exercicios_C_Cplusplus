/*
Faça um programa que receba dois números inteiros e gere os números inteiros
que estão no intervalo compreendido por eles.
*/

#include <stdio.h>

int num1;
int num2;

int main() {

    printf("Número 1°: ");
    scanf("%d", &num1);

    printf("Número 2°: ");
    scanf("%d", &num2);

    for (int i = num1+1; i < num2; i++) {
        printf("%d ", i);
    }

    puts("");
}