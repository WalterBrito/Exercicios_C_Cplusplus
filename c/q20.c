/*
Faça um programa para a leitura de duas notas parciais de um aluno. O programa
deve calcular a média alcançada por aluno e apresentar:
* A mensagem "Aprovado", se a média alcançada for maior ou igual a
sete;
* A mensagem "Reprovado", se a média for menor do que sete;
* A mensagem "Aprovado com Distinção", se a média for igual a dez.
*/

#include <stdio.h>
#include <stdlib.h>

int main() {
    double nota1;
    double nota2;
    double media;

    printf("1° nota: ");
    scanf("%lf", &nota1);
    fflush(stdin);

    printf("2° nota: ");
    scanf("%lf", &nota2);
    fflush(stdin);

    media = (nota1 + nota2) / 2;
    printf("\nMedia: %.1lf\n", media);

    if (media >= 7 && media <= 9) {
        printf("Aprovado!\n");
    } else if (media < 7) {
        printf("Reprovado!\n");
    } else if (media == 10) {
        printf("Aprovado com Distinção.\n");
    }
}

