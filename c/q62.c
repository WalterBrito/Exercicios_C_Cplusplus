// Altere o programa anterior para que ele aceite apenas números entre 0 e 1000.

#include <stdio.h>
#include <stdbool.h>

int numero1;
int numero2;
int numero3;
int soma;
int i = 0;

int main() {

    while (true) {
        printf("1° número: ");
        scanf("%d", &numero1);

        printf("2° número: ");
        scanf("%d", &numero2);

        printf("3° número: ");
        scanf("%d", &numero3);


        if ((numero1 && numero2 && numero3 > 1000)
            || (numero1 && numero2 && numero3 < 0)) {
            printf("\nPermitido somenete numeros de 0 a 1000.\n");
            break;
        }

        if (numero1 > numero2 && numero1 > numero3) {
            printf("\nNúmero %d é o maior.", numero1);
        } else if (numero2 > numero3 && numero2 > numero1) {
            printf("\nNúmero %d é o maior.", numero2);
        } else if (numero3 > numero1 && numero3 > numero2) {
            printf("\nNúmero %d é o maior.", numero3);
        }

        if (numero1 < numero2 && numero1 < numero3) {
            printf("\nNúmero %d é o menor.", numero1);
        } else if (numero2 < numero3 && numero2 < numero1) {
            printf("\nNúmero %d é o menor.", numero2);
        } else if (numero3 < numero1 && numero3 < numero2) {
            printf("\nNúmero %d é o menor.\n", numero3);
        }

        soma = numero1 + numero2 + numero3;
        printf("\nSoma: %d\n", soma);
    }
}