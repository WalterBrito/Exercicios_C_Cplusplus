/*
Faça um Programa que peça as quatro notas de 10 alunos, calcule e armazene num
vetor a média de cada aluno, imprima o número de alunos com média maior ou
igual a 7.0. 
*/

#include <stdio.h>

int main() {
    int aluno[10];
    int nota1, nota2, nota3, nota4;
    int notas[10][10][10][10];
    int media[10];
    int count = 0;

    for (int i = 0; i < 10; ++i) {
        printf("\t-- Aluno %d --\n", i + 1);

        for (int j = 0; j < 1; ++j) {
            printf("Notas: ");
            scanf("%d %d %d %d", &nota1, &nota2, &nota3, &nota4);

            notas[i][i][i][i] = nota1, nota2, nota3, nota4;
        }

        puts("");
    }

    for (int k = 0; k < 10; ++k) {
        printf("\t-- Aluno %d --\n", k + 1);

        for (int i = 0; i < 1; ++i) {
            printf("\n\tNotas");
            for (int j = 0; j < 4; ++j) {
                printf("\n%d", notas[j][j][j][j]);
            }
        }

        puts("");
    }



//    for (int k = 0; k < 10; ++k) {
//        printf("\t-- Aluno %d --\n", k + 1);
//
//        for (int l = 0; l < 1; ++l) {
//            printf("%d ", notas[l][l][l][l]);
//        }
//
//        puts("");
//    }
//
//
//    for (int m = 0; m < 10; ++m) {
//
//        media[m] = notas[m][m][m][m] / 4;
//        if (media[m] >= 7) {
//            count++;
//        }
//    }
//
//    printf("\nMédia Maior ou igual a 7: %d\n", count);
}



