/*
Foram anotadas as idades e alturas de 12 alunos. Faça um Programa que
determine quantos alunos com mais de 13 anos possuem altura inferior à média de
altura desses alunos.
*/

#include <stdio.h>

#define VETOR_IDADE 12
#define VETOR_ALTURA 12


int main() {

    int idade[VETOR_IDADE] = {10, 15, 13, 16, 14, 9, 17, 18, 11, 8, 12, 7};
    int cont = 0;
    int media;

    double altura[VETOR_ALTURA] = {1.10, 1.60, 1.45, 1.78, 1.30, 1.90, 1.85, 1.52, 1.20, 1.53, 1.74, 1.33};

    for (int i = 0; i < VETOR_ALTURA; ++i) {

        // Média dos alunos com mais de 13 anos
        media = (int) VETOR_ALTURA / idade[i];

        if ((idade[i] > 13 && altura[i]) < media) {
            cont++;
        }
    }

    printf("\nMédia: %d\n", cont);

}