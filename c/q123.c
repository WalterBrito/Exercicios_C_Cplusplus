/*
'''Nome na vertical em escada.''' Modifique o programa anterior de forma a
mostrar o nome em formato de escada.
F
FU
FUL
FULA
FULAN
FULANO
*/

#include "string_escada.h"

int main() {

    char palavra[20];

    printf("Digite uma palavra: ");
    scanf("%s", palavra);

    vertical_escada(palavra);
}
