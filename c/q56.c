/*
Faça um programa que peça dois números, base e expoente, calcule e mostre o
primeiro número elevado ao segundo número. Não utilize a função de potência da
linguagem.
*/

#include <stdio.h>

int base;
int expoente;

int main() {
    printf("Base: ");
    scanf("%d", &base);

    printf("Expoente: ");
    scanf("%d", &expoente);

    for (int i = 0; i < 1; i++) {
        printf("\n%d elevado a %d = %d\n", base, expoente, (base * base));
    }

}
