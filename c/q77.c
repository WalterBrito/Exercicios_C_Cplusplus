/*
Encontrar números primos é uma tarefa difícil. Faça um programa que gera uma
lista dos números primos existentes entre 1 e um número inteiro informado pelo
usuário.
*/

#include <stdio.h>

int num;
int i;
int aux = 0;

int main() {

    printf("Digite um número: ");
    scanf("%d", &num);

    for (i = 1; i <= num; i++) {

        if (num % i == 0) {
            aux++;
        }
    }

    printf("%d ", aux);

    puts(" ");

}