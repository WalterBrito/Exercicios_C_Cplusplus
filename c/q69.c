/*
Numa eleição existem três candidatos. Faça um programa que peça o número total
de eleitores. Peça para cada eleitor votar e ao final mostrar o número de votos de
cada candidato.
*/

#include <stdio.h>
#include <stdbool.h>

int qtidade_eleitores;
int voto;
int count;

int main() {

    printf("Digite a quantidade de eleitores: ");
    scanf("%d", &qtidade_eleitores);

    for (int i = 0; i < qtidade_eleitores; i++) {
        printf("Vote: ");
        scanf("%d", &voto);

        if (voto == 1) {
            count++;
        } else if (voto == 2) {
            count++;
        } else if (voto == 3) {
            count++;
        } else {
            printf("Voto inválido");
        }

        voto += count;
    }

    printf("\nCandidato 1: %d voto(s).", voto);
    printf("\nCandidato 2: %d voto(s).", voto);
    printf("\nCandidato 3: %d voto(s).\n", voto);
}