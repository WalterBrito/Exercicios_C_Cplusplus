// Verificador de diigito

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#ifndef EXERCICIOS_VERIFICADOR_DE_DIGITO_H
#define EXERCICIOS_VERIFICADOR_DE_DIGITO_H

#endif //EXERCICIOS_VERIFICADOR_DE_DIGITO_H

void verifica_digto(char* x) {

	for (int i = 0; i < strlen(x); ++i) {
        while (1) {

            if (x[i] == ((x[3] != '.')) || x[i] == (x[7] != '.') || x[i] == (x[11] != '-')) {
                puts("Digito inválido!");
                puts("CPF deve ser no formato 'xxx.xxx.xxx-xx'");
                break;
            } else {
                puts("Digito válido");
            }
        }
	}
}
