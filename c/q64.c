/*
Faça um programa que peça um número inteiro e determine se ele é ou não um
número primo. Um número primo é aquele que é divisível somente por ele mesmo
e por 1.
*/

#include <stdio.h>
#include <stdlib.h>

int i;
int num;
int divisor = 0;

int main() {

    do {
        system("clear");
        printf("Digite um número: ");
        scanf("%d", &num);
    } while (num <= 0);

    for (i = 1; i <= num; i++) {
        if (num % i == 0) {
            divisor++;
        }
    }

    if (divisor == 2) {
        printf("\n%d é primo.\n", num);
    } else {
        printf("\n%d não é primo.\n", num);
        return 0;
    }
}
