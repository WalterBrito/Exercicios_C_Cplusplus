#ifndef EXERCICIOS_Q118_H
#define EXERCICIOS_Q118_H

#endif //EXERCICIOS_Q118_H

#include <string.h>

char dia(int x) {

    char a[2];

    // Converte dia
    if (x > 0 && x <= 31) {
        a[2] = (char) x;
    }

    return a;

}

char mes(int y) {

    char *b[15];

    // Converte mês
    if (y == 1) {
        *b = "Janeiro";
    }

    if (y == 2) {
        *b = "Fevereiro";
    }

    if (y == 3) {
        *b = "Março";
    }

    if (y == 4) {
        *b = "Abril";
    }

    if (y == 5) {
        *b = "Maio";
    }

    if (y == 6) {
        *b = "Junho";
    }

    if (y == 7) {
        *b = "Julho";
    }

    if (y == 8) {
        *b = "Agosto";
    }

    if (y == 9) {
        *b = "Setembro";
    }

    if (y == 10) {
        *b = "Outubro";
    }

    if (y == 11) {
        *b = "Novembro";
    }

    if (y == 12) {
        *b = "Dezembro";
    }

    return b;
}

//char ano(int z) {
//
//    char *c[4];
//
//    int cont = 0;
//    int i = 0;
//    int j = 4;
//
//    while (i < j) {
//        cont++;
//        i++;
//    }
//
//    // Converte ano
//    if (z < 4) {
//        c = (char) z;
//    }
//
//    return c;
//}