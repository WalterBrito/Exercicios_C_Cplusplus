/*
Uma academia deseja fazer um senso entre seus clientes para descobrir o mais
alto, o mais baixo, a mais gordo e o mais magro, para isto você deve fazer um
programa que pergunte a cada um dos clientes da academia seu código, sua altura
e seu peso. O final da digitação de dados deve ser dada quando o usuário digitar 0
(zero) no campo código. Ao encerrar o programa também deve ser informados os
códigos e valores do clente mais alto, do mais baixo, do mais gordo e do mais
magro, além da média das alturas e dos pesos dos clientes
*/

#include <stdio.h>
#include <stdbool.h>

// Variavéis do tipo int
int codigo;
int peso;
int peso_aux; // para saber total dos pesos
int count = 0; // conta a quantidade de pessoas
int media_peso;

// Variavéis do tipo double;
double altura;
double altura_aux; // para saber total das alturas
double media_altura;

// Funções do tipo int
int gordo(int g);
int magro(int m);

// Funções do tipo double
double alto(double a);
double baixo(double b);

void senso() {
    while (true) {
        printf("Código: ");
        scanf("%d", &codigo);

        if (codigo == 0) {
            break;
        }

        printf("Altura: ");
        scanf("%lf", &altura);

        altura_aux += altura;

        printf("Peso: ");
        scanf("%d", &peso);

        peso_aux += peso;

        puts(" ");

        count++;
    }

    printf("\nMais alto: %d - %.2lf", codigo, alto(altura));
    printf("\nMais baixo: %d - %.2lf", codigo, baixo(altura));
    printf("\nMais gordo: %d - %d", codigo, gordo(peso));
    printf("\nMais magro: %d - %d\n", codigo, magro(peso));

    media_altura = altura_aux / count;
    media_peso = peso_aux / count;

    printf("\nMedia de altura: %.2lf", media_altura);
    printf("\nMédia de peso: %d Kg \n", media_peso);
}

double alto(double a) {
    a = altura;

    if (altura < a) {
        a = altura;
    }

    return a;
}


double baixo(double b) {
    b = altura;

    if (altura > b) {
        b = altura;
    }

    return  b;
}

int gordo(int g) {
    g = peso;

    if (peso < g) {
        g = peso;
    }

    return g;
}

int magro(int m) {
    m = peso;

    if (peso > m) {
        m = peso;
    }

    return m;
}

int main() {
    senso();
}