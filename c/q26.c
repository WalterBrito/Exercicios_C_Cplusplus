/*
Faça um programa que recebe o salário de um colaborador e o reajuste segundo
o seguinte critério, baseado no salário atual:

salários até R$ 280,00 (incluindo): aumento 20%
salários entre R$ 280,00 e R$ 700,00: aumento 15%
salários entre R$ 700,00 e R$ 1500,00: aumento 10%
salários de R$ 1500,00 em diante: aumento  5%

Após o aumento ser realizado, informe na tela:
a. o salário antes do reajuste;
b. o percentual de aumento aplicado;
c. o valor do aumento;
d. o novo salário, após o aumento.
*/

#include <stdio.h>

double aumento = 0;
double *aumentoPtr = &aumento;

void aumento_salario() {
    double salario;

    printf("Digite o salaŕio: ");
    scanf("%lf", &salario);

    if (salario < 280) {
        *aumentoPtr = 0.2;
    } else if (salario >= 280 && salario < 700) {
        *aumentoPtr = 0.15;
    } else if(salario >= 700 && salario < 1500) {
        *aumentoPtr = 0.1;
    } else if (salario >= 1500) {
        *aumentoPtr = 0.05;
    }

    printf("\no salário antes do reajuste: %.2lf", salario);
    printf("\no percentual de aumento aplicado: %.2lf%%",  *aumentoPtr);
    printf("\no valor do aumento: R$ %.2lf", salario * *aumentoPtr);
    printf("\no novo salário, após o aumento: R$ %.2lf\n", (salario + (salario * *aumentoPtr)));
}


int main() {
    aumento_salario();
}