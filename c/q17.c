/*
Faça um Programa que verifique se uma letra digitada é "F" ou "M". Conforme a
letra escrever: F - Feminino, M - Masculino, Sexo Inválido.*
*/

#include <stdio.h>
#include <stdlib.h>

int main() {
    char letra;

    printf("Digite uma letra: ");
    scanf("%s", &letra);

    if (letra == 'm') {
        printf("\nM - Masculino\n");
    } else if (letra == 'f') {
        printf("\nF - Feminino\n");
    } else {
        printf("\nSexo Inválido\n");
    }
}