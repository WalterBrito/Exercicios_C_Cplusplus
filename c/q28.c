/*
 Faça um Programa que leia um número e exiba o dia correspondente da semana.
 (1-Domingo, 2- Segunda, etc.), se digitar outro valor deve aparecer valor inválido.
*/

#include <stdio.h>

int num;

int main() {
    printf("Digite um número: ");
    scanf("%d", &num);

    switch (num) {
        case 1:
            printf("\nDomingo\n");
            break;
        case 2:
            printf("\nSegunda\n");
            break;
        case 3:
            printf("\nTerça\n");
            break;
        case 4:
            printf("\nQuarta\n");
            break;
        case 5:
            printf("\nQuinta\n");
            break;
        case 6:
            printf("\nSexta\n");
            break;
        case 7:
            printf("\nSabádo\n");
            break;
        default:
            printf("\nInválido.\n");
            break;
    }
}