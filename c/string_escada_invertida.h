// Função para imprimir string em escada invertida

#ifndef EXERCICIOS_STRING_ESCADA_H
#define EXERCICIOS_STRING_ESCADA_H

#endif //EXERCICIOS_STRING_ESCADA_H

#include <string.h>
#include <stdio.h>

void escada_invertida(char x[20]) {

    // Percorre o vetor
    for (int j = 0; j < strlen(x); ++j) {
        printf("%s\n", &x[j]);
    }
}
