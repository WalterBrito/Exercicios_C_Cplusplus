/*
'''Reverso do número.''' Faça uma função que retorne o reverso de um número
inteiro informado. Por exemplo: 127 -> 721.
*/

#include <stdio.h>

#include "q116.h"

void num_reverso();

int main() {
    num_reverso();
}

void num_reverso() {

    int num;

    printf("Digite um digito (Ex.: 123): ");
    scanf("%d", &num);

    printf("\n%d ~> %d\n", num, reverso(num));
}