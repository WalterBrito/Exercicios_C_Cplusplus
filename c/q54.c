//  Altere o programa anterior para mostrar no final a soma dos números.

#include <stdio.h>

int num1;
int num2;
int soma = 0;

int main() {

    printf("Número 1°: ");
    scanf("%d", &num1);

    printf("Número 2°: ");
    scanf("%d", &num2);

    for (int i = num1+1; i < num2; i++) {
        printf("%d ", i);
        soma += i;
    }

    printf("\nSoma: %d\n", soma);
}

