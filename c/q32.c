/*
Faça um Programa que peça um número correspondente a um determinado ano e
em seguida informe se este ano é ou não bissexto.
*/
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int ano;

int main() {
    printf("Digite o ano: ");
    scanf("%d", &ano);

    if (ano % 4 == 0 || ano % 400 == 0 && ano % 100 != 0) {
        printf("\n%d é bissexto!\n", ano);
    } else {
        printf("\n%d não é bissexto.\n", ano);
    }
}