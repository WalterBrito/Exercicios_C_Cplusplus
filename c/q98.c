/*
Faça um Programa que peça a idade e a altura de 5 pessoas, armazene cada
informação no seu respectivo vetor. Imprima a idade e a altura na ordem inversa a
ordem lida.
*/

#include <stdio.h>

int main() {
    int idade;
    int vetor_idade[5];

    double altura;
    double vetor_altura[5];

    for (int i = 0; i < 5; ++i) {
        printf("Idade: ");
        scanf("%d", &idade);

        vetor_idade[i] = idade;

        printf("Altura: ");
        scanf("%lf", &altura);

        vetor_altura[i] = altura;

        puts("");
    }

    printf("Idade: ");
    for (int j = 4; 0 <= j; j--) {
        printf("%d ", vetor_idade[j]);
    }

    printf("\nAltura: ");
    for (int k = 4; 0 <= k; k--) {
        printf("%.2lf ",  vetor_altura[k]);
    }

    puts("");
}