/*
'''Verificação de CPF.''' Desenvolva um programa que solicite a digitação de um
número de CPF no formato '''xxx.xxx.xxx-xx''' e indique se é um número válido
ou inválido através da validação dos dígitos verificadores e dos caracteres de
formatação.
*/

#include "verificador_de_digito.h"

#define TAMANHO 11

int main() {

    char digito[TAMANHO];

    printf("Número:  ");
    scanf("%s", digito);

    puts("");

    verifica_digto(digito);
}

