/*
Faça um Programa que peça um valor e mostre na tela se o valor é positivo ou
negativo.
*/

#include <stdio.h>
#include <stdlib.h>

int main() {
    int num;

    printf("Digite um  número: ");
    scanf("%d", &num);

    if (num >= 0) {
        printf("\nNúmero positivo!\n");
    } else {
        printf("\nNúmero negativo!\n");
    }
}