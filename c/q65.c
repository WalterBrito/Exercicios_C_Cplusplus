/*
Altere o programa de cálculo dos números primos, informando, caso o número
não seja primo, por quais número ele é divisível.
*/

#include <stdio.h>
#include <stdlib.h>

int i;
int j;
int num;
int divisor = 0;

int main() {

    do {
        system("clear");
        printf("Digite um número: ");
        scanf("%d", &num);
    } while (num <= 0);

    for (i = 1; i <= num; i ++) {
        if (num % i == 0) {
            divisor++;
        }
    }

    if (divisor == 2) {
        printf("\n%d é primo.\n", num);
    } else if (divisor != 2) {
        printf("\n%d não é primo.\n", num);

        printf("\n%d é divisivel por:", num);
        for (j = 0; j <= num; j++) {
            if (num % 2 == 0) {
                printf("\n%d", j++);
            }
        }

        puts("");
    }
}