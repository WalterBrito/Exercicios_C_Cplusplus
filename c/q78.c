/*
Desenvolva um programa que faça a tabuada de um número qualquer inteiro que
será digitado pelo usuário, mas a tabuada não deve necessariamente iniciar em 1 e
terminar em 10, o valor inicial e final devem ser informados também pelo usuário,
conforme exemplo abaixo:
Montar a tabuada de: 5
Começar por: 4
Terminar em: 7

Vou montar a tabuada de 5 começando em 4 e terminando em 7:
5 x 4 = 20
5 x 5 = 25
5 x 6 = 30
5 x 7 = 35

Obs: Você deve verificar se o usuário não digitou o final menor que
o inicial.
*/

#include <stdio.h>

int tabuada;
int inicio;
int fim;
int i;

int main() {

    printf("Tabuada: ");
    scanf("%d", &tabuada);

    printf("Inicio: ");
    scanf("%d", &inicio);

    printf("Fim: ");
    scanf("%d", &fim);

    for (int i = inicio; i <= fim; i++) {

        if (fim < i) {
            printf("\n%d é maior que %d.\n", fim, i);
        }

        printf("\n%d x %d = %d", tabuada, i, (tabuada * i));
    }

    puts(" ");
}