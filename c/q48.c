/*
Altere o programa anterior permitindo ao usuário informar as populações e as
taxas de crescimento iniciais. Valide a entrada e permita repetir a operação.
*/

#include <stdio.h>


int paisA;
int paisB;

double taxa_paisA;
double taxa_paisB;

double anos_necessariosA;
double anos_taxaB;

int main() {


    do {

        printf("Digite a população A: ");
        scanf("%d", &paisA);

        printf("Digite a taxa de crescimento A: ");
        scanf("%lf", &taxa_paisA);

        puts("");

        printf("Digite a população B: ");
        scanf("%d", &paisB);

        printf("Digite a taxa de crescimento B: ");
        scanf("%lf", &taxa_paisB);

        anos_necessariosA = paisA * taxa_paisA;
        anos_taxaB = paisB * taxa_paisB;

        printf("\nPais A ultrapassará ou igualará pais B em %d anos.\n",
               (int) anos_necessariosA);

        puts("Digite zero para sair.");
        printf("Deseja continuar?: ");
        scanf("%d", &paisA);

        if (paisA == 0) {
            break;
        }
    } while (anos_necessariosA >= anos_taxaB);
}