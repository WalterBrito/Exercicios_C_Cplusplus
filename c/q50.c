// Faça um programa que leia 5 números e informe o maior número.

#include <stdio.h>

int num1;
int num2;
int num3;
int num4;
int num5;

int main() {
    for (int i = 0; i <= 0; i++) {
        printf("Número 1: ");
        scanf("%d", &num1);

        printf("Número 2: ");
        scanf("%d", &num2);

        printf("Número 3: ");
        scanf("%d", &num3);

        printf("Número 4: ");
        scanf("%d", &num4);

        printf("Número 5: ");
        scanf("%d", &num5);

        if (num1 > num2 && num1 > num3 && num1 > num4
            && num1 > num5) {
            printf("\n%d é o maior.\n", num1);
        } else if (num2 > num3 && num2 > num4 && num2 > num5
                   && num2 > num1) {
            printf("\n%d é o maior.\n", num2);
        } else if (num3 > num4 && num3 > num5 && num3 > num1
                   && num3 > num1) {
            printf("\n%d é o maior.\n", num3);
        } else if (num4 > num5 && num4 > num1 && num4 > num2
                   && num4 > num3) {
            printf("\n%d é o maior.\n", num4);
        } else if (num5 > num1 && num5 > num2 && num5 > num3
                   && num5 > num4) {
            printf("\n%d é o maior.\n", num5);
        }
    }
}


