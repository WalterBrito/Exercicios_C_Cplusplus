/*
Um posto está vendendo combustíveis com a seguinte tabela de
descontos:
a. Álcool:
b. até 20 litros, desconto de 3% por litro
c. acima de 20 litros, desconto de 5% por litro
d. Gasolina:
e. até 20 litros, desconto de 4% por litro
f. acima de 20 litros, desconto de 6% por litro

Escreva um algoritmo que leia o número de litros vendidos, o tipo de
combustível (codificado da seguinte forma: A-álcool, G-gasolina), calcule e
imprima o valor a ser pago pelo cliente sabendo-se que o preço do litro da
gasolina é R$ 2,50 o preço do litro do álcool é R$ 1,90.
*/

#include <stdio.h>

int qtidade_litros_alcool;
int qtidade_litros_gasolina;

double preco_alcool = 1.90;
double preco_gasolina = 2.50;
double total_alcool;
double total_gasolina;

void descontos() {
    printf("Quatidade de alcool: ");
    scanf("%d", &qtidade_litros_alcool);

    printf("Quantidade de gasolina: ");
    scanf("%d", &qtidade_litros_gasolina);

    if (qtidade_litros_alcool <= 20) {
        total_alcool = (qtidade_litros_alcool * preco_alcool) - 0.03;
    } else if (qtidade_litros_alcool >= 20) {
        total_alcool = (qtidade_litros_alcool * preco_alcool) - 0.05;
    }

    if (qtidade_litros_gasolina <= 20) {
        total_gasolina = (qtidade_litros_gasolina * preco_gasolina) - 0.04;
    } else if (qtidade_litros_gasolina >= 20) {
        total_gasolina = (qtidade_litros_gasolina * preco_gasolina) - 0.06;
    }

    printf("\nTotal Alcool: %.2lf\n", total_alcool);
    printf("Total Gasolina: %.2lf\n", total_gasolina);
}

int main() {
    descontos();
}