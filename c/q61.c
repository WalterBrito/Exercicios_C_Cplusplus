/*
Faça um programa que, dado um conjunto de N números, determine o menor
valor, o maior valor e a soma dos valores.
*/

#include <stdio.h>

int numero1;
int numero2;
int numero3;
int soma;

int main(void) {

    for (int i = 1; i < 2; i++) {
        printf("%d número: ", i);
        scanf("%d", &numero1);

        printf("%d número: ", i);
        scanf("%d", &numero2);

        printf("%d número: ", i);
        scanf("%d", &numero3);
    }

    if (numero1 > numero2 && numero1 > numero3) {
        printf("\nNúmero %d é o maior.", numero1);
    } else if (numero2 > numero3 && numero2 > numero1) {
        printf("\nNúmero %d é o maior.", numero2);
    } else if (numero3 > numero1 && numero3 > numero2) {
        printf("\nNúmero %d é o maior.", numero3);
    }

    if (numero1 < numero2 && numero1 < numero3) {
        printf("\nNúmero %d é o menor.", numero1);
    } else if (numero2 < numero3 && numero2 < numero1) {
        printf("\nNúmero %d é o menor.", numero2);
    } else if (numero3 < numero1 && numero3 < numero2) {
        printf("\nNúmero %d é o menor.\n", numero3);
    }

    soma = numero1 + numero2 + numero3;
    printf("\nSoma: %d\n", soma);
}