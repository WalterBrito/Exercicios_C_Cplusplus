#ifndef EXERCICIOS_Q117_H
#define EXERCICIOS_Q117_H

#endif //EXERCICIOS_Q117_H

#include <stdlib.h>

int rand(void);

int craps(int x) {

    int y = 0;
    int i = 2;

    y = rand();

    while (i <= 11) {
        if (y == x) {
            break;
        }

        if (y == 7 || y == 11) {
           y = x;
        } else if (y == 2 || y == 3 || y == 12) {
            y = x;
        } else if (y == 4 || y == 5 || y == 6 || y == 8 || y == 9 || y == 10) {
            y = x;
        }

        i++;
    }

    return y;
}