/*
Faça um Programa que leia um vetor de 5 números inteiros, mostre a soma, a
multiplicação e os números.
*/

#include <stdio.h>

int main() {
    int vetor[5] = {2, 3, 4, 5, 6};
    int soma = 0;
    int multiplicacao = 1;

    for (int i = 0; i < 5; i++) {
        soma += vetor[i];
    }

    printf("\nSoma: %d", soma);

    for (int j = 0; j < 5; ++j) {
        multiplicacao *= vetor[j];
    }

    printf("\nmultiplicacao: %d\n", multiplicacao);

    printf("Vetor: ");
    for (int k = 0; k < 5; ++k) {
        printf("%d ", vetor[k]);
    }

    puts("");
}

