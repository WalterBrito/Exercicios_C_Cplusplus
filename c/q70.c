/*
Faça um programa que calcule o número médio de alunos por turma. Para isto,
peça a quantidade de turmas e a quantidade de alunos para cada turma. As turmas
não podem ter mais de 40 alunos.
*/

#include <stdio.h>

int turmas;
int alunos;

int alunos_por_turmas;

int main() {
    printf("Quantidade de turmas: ");
    scanf("%d", &turmas);

    printf("Quantidade de alunos: ");
    scanf("%d", &alunos);

    for (int i = 0; i < 40; i++) {
        alunos_por_turmas++;
    }

    alunos_por_turmas = alunos / turmas;

    if (alunos_por_turmas > 40) {
        printf("\nAlunos por turmas: %d\n", alunos_por_turmas);
        printf("\nNúmero de alunos por turma execedente.\n");
    } else {
        printf("\nAlunos por turmas: %d\n", alunos_por_turmas);
    }

}