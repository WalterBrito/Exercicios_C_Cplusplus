/*
Uma fruteira está vendendo frutas com a seguinte tabela de preços:
                Até 5 Kg            Acima de 5 Kg
Morango         R$ 2,50 por Kg      R$ 2,20 por Kg
Maçã            R$ 1,80 por Kg      R$ 1,50 por Kg

Se o cliente comprar mais de 8 Kg em frutas ou o valor total da compra
ultrapassar R$ 25,00, receberá ainda um desconto de 10% sobre este total.
Escreva um algoritmo para ler a quantidade (em Kg) de morangos e a quantidade
(em Kg) de maças adquiridas e escreva o valor a ser pago pelo cliente.
*/

#include <stdio.h>

int qtidade_morango;
int qtidade_maca;

double preco_morango;
double preco_maca;
double total = 0;

void tabela() {
    printf("Quantidade de morango (Kg): ");
    scanf("%d", &qtidade_morango);

    printf("Quantidade de maca (Kg): ");
    scanf("%d", &qtidade_maca);

    if (qtidade_morango <= 5) {
        preco_morango = qtidade_morango * 2.50;
        printf("\n%d Kg de morango: R$ %.2lf\n", qtidade_morango, preco_morango);
    } else {
        preco_morango = qtidade_morango * 2.20;
        printf("\n%d Kg de morango: R$ %.2lf\n", qtidade_morango, preco_morango);
    }

    if (qtidade_maca <= 5) {
        preco_maca = qtidade_maca * 1.80;
        printf("%d Kg de maca: R$ %.2lf\n", qtidade_maca, preco_maca);
    } else {
        preco_maca = qtidade_maca * 1.50;
        printf("%d Kg de maca: R$ %.2lf\n", qtidade_maca, preco_maca);
    }

    if ((qtidade_morango + qtidade_maca) > 8 || total > 25) {
        total = (qtidade_morango + preco_morango) * 0.1;
    } else {
        total = qtidade_morango + preco_morango;
    }

    printf("\nTotal: R$ %.2f\n", total);
}

int main() {
    tabela();
}
