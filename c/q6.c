// Faça um Programa que peça o raio de um círculo, calcule e mostre sua área.

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void area() {
  // double r;
  double raio;
  double PI = 3.14;
  double area_circulo;

  printf("\t Area do circulo.\n");
  printf("Digite o raio: ");
  scanf("%lf", &raio);

  area_circulo = PI * (raio * raio);
  printf("Area do circulo ~> %.2lf\n", area_circulo);
}

int main() {
  area();
}
