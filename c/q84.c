/*
Faça um programa que leia uma quantidade indeterminada de números positivos e
conte quantos deles estão nos seguintes intervalos: [0-25], [26-50], [51-75] e [76-
100]. A entrada de dados deverá terminar quando for lido um número negativo.
*/

#include <stdio.h>
#include <stdbool.h>

int num;
int cont = 0;
int a, b, c, d = 0;

int main() {

    while (true) {
        printf("Digite um número: ");
        scanf("%d", &num);

        if (num < 0) {
            break;
        }

        if (num >= 0 && num <= 25) {
            a++;
        }

        if (num >= 26 && num <= 50) {
            b++;
        }

        if (num >= 51 && num <= 75) {
            c++;
        }

        if (num >= 76 && num <= 100) {
            d++;
        }

    }

    printf("\n[0-25]: %d", a);
    printf("\n[26-50]: %d", b);
    printf("\n[51-75]: %d", c);
    printf("\n[76-100]: %d\n", d);
}
