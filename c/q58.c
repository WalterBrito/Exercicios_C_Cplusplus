/*
A série de Fibonacci é formada pela seqüência 1,1,2,3,5,8,13,21,34,55,... Faça um
programa capaz de gerar a série até o n−ésimo termo.
*/

#include <stdio.h>

int main() {
    int a;
    int b;
    int aux;
    int n;

    a = 0;
    b = 1;

    printf("Digite um número: ");
    scanf("%d, ", &n);

    printf("\n\tSérie de Fibonacci:\n");
    printf("%d, ", b);

    for (int i = 0; i < n; i++) {
        aux = a + b;
        a = b;
        b = aux;

        printf("%d, ", aux);
    }
}

