/*
Faça um Programa para um caixa eletrônico. O programa deverá perguntar ao
usuário a valor do saque e depois informar quantas notas de cada valor serão
fornecidas. As notas disponíveis serão as de 1, 5, 10, 50 e 100 reais. O valor
mínimo é de 10 reais e o máximo de 600 reais. O programa não deve se preocupar
com a quantidade de notas existentes na máquina.

a. Exemplo 1: Para sacar  a quantia de 256 reais, o programa fornece
duas notas de 100, uma nota de 50, uma nota de 5 e uma nota de 1;
b. Exemplo 2: Para sacar a quantia de 399 reais, o programa fornece
três notas de 100, uma nota de 50, quatro notas de 10, uma nota de 5 e
quatro notas de 1.
*/

#include <stdio.h>
#include <string.h>

int valor_a_sacar;
int nota1;
int nota5;
int nota10;
int nota50;
int nota100;
int resto1;
int resto5;
int resto10;
int resto50;
int resto100;

void saque() {
    printf("Valor a sacar: ");
    scanf("%d", &valor_a_sacar);

    if (valor_a_sacar >= 10 && valor_a_sacar <= 600) {
        // Notas de 100
        nota100 = valor_a_sacar / 100;
        resto100 = valor_a_sacar % 100;

        // Notas de 50
        nota50 = resto100 / 50;
        resto50 = resto100 % 50;

        // Notas de 10
        nota10 = resto50 / 10;
        resto10 = resto100 % 10;

        // Notas de 5
        nota5 = resto10 / 5;
        resto5 = resto10 % 5;

        // Notas de 1
        nota1 = resto5 / 1;
        resto1 = resto5 % 1;
    } else {
        puts("Saque não permitido.");
    }

    printf("\n%d Nota(s) de R$ 100", nota100);
    printf("\n%d Nota(s) de R$ 50", nota50);
    printf("\n%d Nota(s) de R$ 10", nota10);
    printf("\n%d Nota(s) de R$ 5", nota5);
    printf("\n%d Nota(s) de R$ 1\n", nota1);
}

int main() {
    saque();
}