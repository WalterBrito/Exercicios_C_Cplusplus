/*
Faça um Programa que pergunte quanto você ganha por hora e o número de horas
trabalhadas no mês. Calcule e mostre o total do seu salário no referido mês,
sabendo-se que são descontados 11% para o Imposto de Renda, 8% para o INSS e
5% para o sindicato, faça um programa que nos dê:
a. salário bruto.
b. quanto pagou ao INSS.
c. quanto pagou ao sindicato.
d. o salário líquido.
e. calcule os descontos e o salário líquido, conforme a tabela
abaixo:
+ Salário Bruto : R$
- IR (11%) : R$
- INSS (8%) : R$
- Sindicato ( 5%) : R$
= Salário Liquido : R$

Obs.: Salário Bruto - Descontos = Salário Líquido.
*/

#include <stdio.h>
#include <stdlib.h>

double salario_bruto;
double salario_liquido;
double ganho_por_hora;
int horas_trabalhadas;
double imposto_de_renda = 0.11;
double inss = 0.08;
double sindicato = 0.05;
double desconto;

void tabela() {
    printf("\n\tTabela de descontos\n");
    printf("\nImposto de Renda: 5%%");
    printf("\nINSS: 8%%");
    printf("\nSindicato: 5%%\n");
}

void descontos() {
    printf("\nValor da hora: ");
    scanf("%lf", &ganho_por_hora);

    printf("Quantidade de horas trabalhadas: ");
    scanf("%d", &horas_trabalhadas);

    salario_bruto = (ganho_por_hora + horas_trabalhadas) * 30;
    printf("\nSalário bruto: %.2lf", salario_bruto);

    desconto = ((salario_bruto * imposto_de_renda) + (salario_bruto * inss)  + (salario_bruto * sindicato));

    salario_liquido = (salario_bruto - desconto);
    printf("\nSalário líquido: %.2lf\n", salario_liquido);
}

int main() {
    tabela();
    descontos();
}